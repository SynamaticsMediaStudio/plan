-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 16, 2019 at 05:24 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `football`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `username` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `player_evaluation`
--

CREATE TABLE `player_evaluation` (
  `fld_id` int(11) NOT NULL,
  `player_number` text NOT NULL,
  `fld_player_name` text NOT NULL,
  `fld_player_father_name` text NOT NULL,
  `fld_player_Nationality` text NOT NULL,
  `fld_player_position` text NOT NULL,
  `fld_player_evaluation` text NOT NULL,
  `fld_player_academy` text NOT NULL,
  `fld_national_team` text NOT NULL,
  `fld_player_contact_number` text NOT NULL,
  `fld_player_father_contact` text NOT NULL,
  `fld_player_foot_info` text NOT NULL,
  `fld_player_image` text NOT NULL,
  `fld_cat` text NOT NULL,
  `fld_season` text NOT NULL,
  `fld_tactical` text NOT NULL,
  `fld_tac_arabic` text NOT NULL,
  `fld_tac_eval` text NOT NULL,
  `fld_technical` text NOT NULL,
  `fld_tech_arabic` text NOT NULL,
  `fld_tech_eval` text NOT NULL,
  `fld_physical` text NOT NULL,
  `fld_phy_arabic` text NOT NULL,
  `fld_phy_eval` text NOT NULL,
  `fld_psychosocial` text NOT NULL,
  `fld_psy_arabic` text NOT NULL,
  `fld_psy_eval` text NOT NULL,
  `fld_remarks` text NOT NULL,
  `fld_player_sel` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player_evaluation`
--

INSERT INTO `player_evaluation` (`fld_id`, `player_number`, `fld_player_name`, `fld_player_father_name`, `fld_player_Nationality`, `fld_player_position`, `fld_player_evaluation`, `fld_player_academy`, `fld_national_team`, `fld_player_contact_number`, `fld_player_father_contact`, `fld_player_foot_info`, `fld_player_image`, `fld_cat`, `fld_season`, `fld_tactical`, `fld_tac_arabic`, `fld_tac_eval`, `fld_technical`, `fld_tech_arabic`, `fld_tech_eval`, `fld_physical`, `fld_phy_arabic`, `fld_phy_eval`, `fld_psychosocial`, `fld_psy_arabic`, `fld_psy_eval`, `fld_remarks`, `fld_player_sel`) VALUES
(4, '8', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '', '5', '5', 'kl', 'jk', 'kjhj', 'kjjk', 'jkhjk', 'jkj', 'jkjk', 'jkj', 'jkjk', 'kjjk', 'jkhj', 'kjhjk', 'jkklj', 'yes'),
(7, '10', 'SHAHZAIB', 'Shoukat', 'pakistani', 'evaluation', 'football', 'position4', 'PAKISTANI', '8373632R2', '838739', 'Right', '', 'cato', 'U19', 'xkcjkx', 'as,dfk', 'klasdf', 'asdfkla', 'alksdf', 'lkasdf', 'askldjf', 'aksdf', 'alskdf', 'akdmsf', 'akljsdf', 'adkfm', 'sdkjfklasdjfk', 'yes'),
(8, '2', 'taha', 'asdf', 'asdf', 'fasdff', 'asd', 'asdf', 'asdf', 'asdf', 'asdf', 'fasf', '', 'asdf', 'first', 'sam', 'samara', 'sameval', 'samtec', 'samara', 'samtecara', 'samphy', 'samara', 'sameval', 'sampsy', 'sampsyara', 'sampsyeval', 'asdfas', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `player_info`
--

CREATE TABLE `player_info` (
  `fld_id` int(11) NOT NULL,
  `player_number` text NOT NULL,
  `fld_player_name` text NOT NULL,
  `fld_player_father_name` text NOT NULL,
  `fld_player_Nationality` text NOT NULL,
  `fld_player_position` text NOT NULL,
  `fld_player_evaluation` text NOT NULL,
  `fld_player_academy` text NOT NULL,
  `fld_national_team` text NOT NULL,
  `fld_player_contact_number` text NOT NULL,
  `fld_player_father_contact` text NOT NULL,
  `fld_player_foot_info` text NOT NULL,
  `fld_player_image` text NOT NULL,
  `fld_cat` text NOT NULL,
  `fld_season` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player_info`
--

INSERT INTO `player_info` (`fld_id`, `player_number`, `fld_player_name`, `fld_player_father_name`, `fld_player_Nationality`, `fld_player_position`, `fld_player_evaluation`, `fld_player_academy`, `fld_national_team`, `fld_player_contact_number`, `fld_player_father_contact`, `fld_player_foot_info`, `fld_player_image`, `fld_cat`, `fld_season`) VALUES
(10, '6', 'shahzaib', 'shoukat', 'pakistani', '2nd', 'sadfa', 'aspire', 'pakistani', '23423456', '435466', 'right', 'player_images/logo.png', 'cat', 'u56');

-- --------------------------------------------------------

--
-- Table structure for table `weekly_plan`
--

CREATE TABLE `weekly_plan` (
  `fld_id` int(11) NOT NULL,
  `fld_season` text NOT NULL,
  `fld_age_group` text NOT NULL,
  `fld_week` text NOT NULL,
  `fld_month` text NOT NULL,
  `fld_image` text NOT NULL,
  `fld_sun_date` text NOT NULL,
  `fld_mon_date` text NOT NULL,
  `fld_tues_date` text NOT NULL,
  `fld_wed_date` text NOT NULL,
  `fld_thu_date` text NOT NULL,
  `fld_fri_date` text NOT NULL,
  `fld_sat_date` text NOT NULL,
  `fld_sun_intensity` text NOT NULL,
  `fld_mon_intensity` text NOT NULL,
  `fld_tues_intensity` text NOT NULL,
  `fld_wed_intensity` text NOT NULL,
  `fld_thu_intensity` text NOT NULL,
  `fld_fri_intensity` text NOT NULL,
  `fld_sat_intensity` text NOT NULL,
  `fld_sunday_aim` text NOT NULL,
  `fld_monday_aim` text NOT NULL,
  `fld_tues_aim` text NOT NULL,
  `fld_wed_aim` text NOT NULL,
  `fld_thurs_aim` text NOT NULL,
  `fld_fri_aim` text NOT NULL,
  `fld_sat_aim` text NOT NULL,
  `fld_scnd_sunday_aim` text NOT NULL,
  `fld_scnd_monday_aim` text NOT NULL,
  `fld_scnd_tues_aim` text NOT NULL,
  `fld_scnd_wed_aim` text NOT NULL,
  `fld_scnd_thurs_aim` text NOT NULL,
  `fld_scnd_fri_aim` text NOT NULL,
  `fld_scnd_sat_aim` text NOT NULL,
  `fld_sunday_detail` text NOT NULL,
  `fld_mon_detail` text NOT NULL,
  `fld_tues_detail` text NOT NULL,
  `fld_wed_detail` text NOT NULL,
  `fld_thurs_detail` text NOT NULL,
  `fld_fri_detail` text NOT NULL,
  `fld_sat_detail` text NOT NULL,
  `fld_fst_sun_warm_up` text NOT NULL,
  `fld_fst_sun_phy_part` text NOT NULL,
  `fld_fst_sun_coach_part` text NOT NULL,
  `fld_fst_sun_cool_part` text NOT NULL,
  `fld_fst_sun_total` text NOT NULL,
  `fld_fst_sun_rpe_part` text NOT NULL,
  `fld_scnd_sun_warm_up` text NOT NULL,
  `fld_scnd_sun_phy_part` text NOT NULL,
  `fld_scnd_sun_coach_part` text NOT NULL,
  `fld_scnd_sun_cool_part` text NOT NULL,
  `fld_scnd_sun_total` text NOT NULL,
  `fld_scnd_sun_rpe_part` text NOT NULL,
  `fld_third_sun_warm_up` text NOT NULL,
  `fld_third_sun_phy_part` text NOT NULL,
  `fld_third_sun_coach_part` text NOT NULL,
  `fld_third_sun_cool_part` text NOT NULL,
  `fld_third_sun_total` text NOT NULL,
  `fld_third_sun_rpe_part` text NOT NULL,
  `fld_fourth_sun_warm_up` text NOT NULL,
  `fld_fourth_sun_phy_part` text NOT NULL,
  `fld_fourth_sun_coach_part` text NOT NULL,
  `fld_fourth_sun_cool_part` text NOT NULL,
  `fld_fourth_sun_total` text NOT NULL,
  `fld_fourth_sun_rpe_part` text NOT NULL,
  `fld_five_sun_warm_up` text NOT NULL,
  `fld_five_sun_phy_part` text NOT NULL,
  `fld_five_sun_coach_part` text NOT NULL,
  `fld_five_sun_cool_part` text NOT NULL,
  `fld_five_sun_total` text NOT NULL,
  `fld_five_sun_rpe_part` text NOT NULL,
  `fld_six_sun_warm_up` text NOT NULL,
  `fld_six_sun_phy_part` text NOT NULL,
  `fld_six_sun_coach_part` text NOT NULL,
  `fld_six_sun_cool_part` text NOT NULL,
  `fld_six_sun_total` text NOT NULL,
  `fld_six_sun_rpe_part` text NOT NULL,
  `fld_seven_sun_warm_up` text NOT NULL,
  `fld_seven_sun_phy_part` text NOT NULL,
  `fld_seven_sun_coach_part` text NOT NULL,
  `fld_seven_sun_cool_part` text NOT NULL,
  `fld_seven_sun_total` text NOT NULL,
  `fld_seven_sun_rpe_part` text NOT NULL,
  `fld_tactical_first` text NOT NULL,
  `fld_tactical_scnd` text NOT NULL,
  `fld_tactical_third` text NOT NULL,
  `fld_technical_first` text NOT NULL,
  `fld_technical_scnd` text NOT NULL,
  `fld_technical_third` text NOT NULL,
  `fld_mental_first` text NOT NULL,
  `fld_mental_scnd` text NOT NULL,
  `fld_mental_third` text NOT NULL,
  `fld_tactical_fourth` text NOT NULL,
  `fld_tactical_fifth` text NOT NULL,
  `fld_tactical_six` text NOT NULL,
  `fld_technical_fourth` text NOT NULL,
  `fld_technical_fifth` text NOT NULL,
  `fld_technical_six` text NOT NULL,
  `fld_mental_fourth` text NOT NULL,
  `fld_mental_fifth` text NOT NULL,
  `fld_mental_six` text NOT NULL,
  `fld_tactical_seven` text NOT NULL,
  `fld_technical_seven` text NOT NULL,
  `fld_mental_seven` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weekly_plan`
--

INSERT INTO `weekly_plan` (`fld_id`, `fld_season`, `fld_age_group`, `fld_week`, `fld_month`, `fld_image`, `fld_sun_date`, `fld_mon_date`, `fld_tues_date`, `fld_wed_date`, `fld_thu_date`, `fld_fri_date`, `fld_sat_date`, `fld_sun_intensity`, `fld_mon_intensity`, `fld_tues_intensity`, `fld_wed_intensity`, `fld_thu_intensity`, `fld_fri_intensity`, `fld_sat_intensity`, `fld_sunday_aim`, `fld_monday_aim`, `fld_tues_aim`, `fld_wed_aim`, `fld_thurs_aim`, `fld_fri_aim`, `fld_sat_aim`, `fld_scnd_sunday_aim`, `fld_scnd_monday_aim`, `fld_scnd_tues_aim`, `fld_scnd_wed_aim`, `fld_scnd_thurs_aim`, `fld_scnd_fri_aim`, `fld_scnd_sat_aim`, `fld_sunday_detail`, `fld_mon_detail`, `fld_tues_detail`, `fld_wed_detail`, `fld_thurs_detail`, `fld_fri_detail`, `fld_sat_detail`, `fld_fst_sun_warm_up`, `fld_fst_sun_phy_part`, `fld_fst_sun_coach_part`, `fld_fst_sun_cool_part`, `fld_fst_sun_total`, `fld_fst_sun_rpe_part`, `fld_scnd_sun_warm_up`, `fld_scnd_sun_phy_part`, `fld_scnd_sun_coach_part`, `fld_scnd_sun_cool_part`, `fld_scnd_sun_total`, `fld_scnd_sun_rpe_part`, `fld_third_sun_warm_up`, `fld_third_sun_phy_part`, `fld_third_sun_coach_part`, `fld_third_sun_cool_part`, `fld_third_sun_total`, `fld_third_sun_rpe_part`, `fld_fourth_sun_warm_up`, `fld_fourth_sun_phy_part`, `fld_fourth_sun_coach_part`, `fld_fourth_sun_cool_part`, `fld_fourth_sun_total`, `fld_fourth_sun_rpe_part`, `fld_five_sun_warm_up`, `fld_five_sun_phy_part`, `fld_five_sun_coach_part`, `fld_five_sun_cool_part`, `fld_five_sun_total`, `fld_five_sun_rpe_part`, `fld_six_sun_warm_up`, `fld_six_sun_phy_part`, `fld_six_sun_coach_part`, `fld_six_sun_cool_part`, `fld_six_sun_total`, `fld_six_sun_rpe_part`, `fld_seven_sun_warm_up`, `fld_seven_sun_phy_part`, `fld_seven_sun_coach_part`, `fld_seven_sun_cool_part`, `fld_seven_sun_total`, `fld_seven_sun_rpe_part`, `fld_tactical_first`, `fld_tactical_scnd`, `fld_tactical_third`, `fld_technical_first`, `fld_technical_scnd`, `fld_technical_third`, `fld_mental_first`, `fld_mental_scnd`, `fld_mental_third`, `fld_tactical_fourth`, `fld_tactical_fifth`, `fld_tactical_six`, `fld_technical_fourth`, `fld_technical_fifth`, `fld_technical_six`, `fld_mental_fourth`, `fld_mental_fifth`, `fld_mental_six`, `fld_tactical_seven`, `fld_technical_seven`, `fld_mental_seven`) VALUES
(15, 'new', 'new', 'new', 'new', 'upload/3.jpg', 'October 1', '', 'October 2', '', 'October 13', 'October 2', '', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '5', '5', '5', '5', '20', '5', '5', '5', '', '5', '20', '5', '5', '5', '5', '5', '20', '5', '5', '5', '5', '5', '20', '5', '5', '', '5', '5', '20', '5', '5', '5', '5', '5', '20', '5', '5', '5', '55', '5', '20', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5'),
(16, 'sdfsd', 'asdfa', 'dfasdf', 'March', 'upload/14.jpg', '4', '12', '9', '7', '3', '4', '5', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '5', '5', '5', '5', '20', '5', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `player_evaluation`
--
ALTER TABLE `player_evaluation`
  ADD PRIMARY KEY (`fld_id`);

--
-- Indexes for table `player_info`
--
ALTER TABLE `player_info`
  ADD PRIMARY KEY (`fld_id`);

--
-- Indexes for table `weekly_plan`
--
ALTER TABLE `weekly_plan`
  ADD PRIMARY KEY (`fld_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `player_evaluation`
--
ALTER TABLE `player_evaluation`
  MODIFY `fld_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `player_info`
--
ALTER TABLE `player_info`
  MODIFY `fld_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `weekly_plan`
--
ALTER TABLE `weekly_plan`
  MODIFY `fld_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

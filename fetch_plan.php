<?php
include_once("secure.php");
  error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Pro Football Coach</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!--  Fremwork -->
    
    <!-- ico font -->
    
</head>

<?php
  include('dbbridge/top.php');
  $db = new DBManager();

  //Prepaare

  $query      = "SELECT * FROM weekly_plan";
  $result     = $db->sample($query);
  $fld_season = $result;




?>

<body  class="" text="green">
    
<div  class="page-wrap" style="overflow: hidden;">
    <section class="nav_section ">
        <div class="container-fluid">
            <form class="form-control " name="form" method="post" enctype="multipart/form-data" id="plan_data" action="update_weekly_plan.php">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="images/Pro_Football_Coach_Logo.png"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">      
        <img class="client_logo">
      </li>
    </ul>
  </div>
</nav>
<div class="card card-body border-0 shadow-sm">
<div class="row">
  <!-- FLD Season -->
  <div class="col-sm-4 col-md-4 col-lg-3">
    <div class="form-group">
      <label for="fld_season_select">Season</label>
      <select class="form-control" name="fld_season_select" id="fld_season_select">
          <?php foreach ($fld_season as $fds) : ?>
            <option><?php echo $fds['fld_season'];?></option>
          <?php endforeach;?>
      </select>
    </div>
  </div>
  <!-- FLD Season -->

  <!-- FLD Season -->
  <div class="col-sm-4 col-md-4 col-lg-3">
    <div class="form-group">
      <label for="fld_age_group_select">Age Group</label>
      <select class="form-control" name="fld_age_group_select" id="fld_age_group_select">
          <?php foreach ($fld_season as $fds) : ?>
            <option><?php echo $fds['fld_age_group'];?></option>
          <?php endforeach;?>
      </select>
    </div>
  </div>
  <!-- FLD Season -->

  <!-- FLD Season -->
  <div class="col-sm-4 col-md-4 col-lg-3">
    <div class="form-group">
      <label for="fld_week_select">Week</label>
      <select class="form-control" name="fld_week_select" id="fld_week_select">
          <?php foreach ($fld_season as $fds) : ?>
            <option><?php echo $fds['fld_week'];?></option>
          <?php endforeach;?>
      </select>
    </div>
  </div>
  <!-- FLD Season -->

  <!-- FLD Season -->
  <div class="col-sm-4 col-md-4 col-lg-3">
    <div class="form-group">
      <label for="fld_month_select">Month</label>
      <select class="form-control" name="fld_month_select" id="fld_month_select">
          <?php foreach ($fld_season as $fds) : ?>
            <option><?php echo $fds['fld_month'];?></option>
          <?php endforeach;?>
      </select>
    </div>
  </div>
  <!-- FLD Season -->
</div>
</div>



<button type="button" id="download" class="btn btn-info" style="float: right;margin: 0 0 10px 0; margin: 40px 0 10px 0;">Download</button>
<input type="submit" name="update" value="Update" class="btn btn-light" style="float: right;margin: 0 0 10px 0; margin: 40px 15px 10px 0;">
<input type="submit" name="delete" value="Delete" class="btn btn-danger" style="float: right;margin: 0 0 10px 0; margin: 40px 15px 10px 0;" >
            <h1 align="top">Weekly Plan</h1>
            <div>
            <div class="table-responsive ">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Day</th>
                            <th scope="col">Sunday</th>
                            <th scope="col">Monday</th>
                            <th scope="col">Tuesday</th>
                            <th scope="col">Wednesday</th>
                            <th scope="col">Thursday</th>
                            <th scope="col">Friday</th>
                            <th scope="col">Saturday</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="first_tb_data">Date</td>
                            <td><input type="text" id="no-year-datepicker" class="set_date sun_date form-control" name="sun_date"></td>
                            <td>
                            <input type="text" id="monday_date" class="set_date sun_date form-control" name="mon_date">
                            </td>
                            <td>
                                <input type="text" id="tuesday_date" class="set_date sun_date form-control" name="tues_date">
                                <!-- <input type="text" id="tuesday_date" class="set_date"  name="tuesday_date"> --></td>
                            <td><input type="text" id="wed_date" class="set_date form-control"  name="wed_date"></td>
                            <td><input type="text" id="thursday_date" class="set_date form-control"  name="thursday_date"></td>
                            <td><input type="text" id="friday_date" class="set_date form-control"  name="friday_date"></td>
                            <td><input type="text" id="sat_date" class="set_date form-control"  name="sat_date"></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Intensity</td>
                            <td class="color_align">
                            <select name="sun_color"  id="sun_color">
                                <!-- <option id="appended"></option> -->
                                <option value="#fff">White</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="mon_color"  id="mon_color" class="color_align">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="tues_color"  id="tues_color">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="wed_color"  id="wed_color">
                               <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="thu_color"  id="thu_color">
                              <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="fri_color"  id="fri_color">
                               <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="sat_color"  id="sat_color">
                            <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Aim</td>
                            <td><input type="text" name = "sunday_aim" class="form-control" reqired id="1st_sun_aim"></td>
                            <td><input type="text" name = "monday_aim" class="form-control"  id="1st_mon_aim"></td>
                            <td><input type="text" name = "tues_aim" class="form-control"  id="1st_tue_aim"></td>
                            <td><input type="text" name = "wed_aim" class="form-control"  id="1st_wed_aim"></td>
                            <td><input type="text" name = "thurs_aim" class="form-control"  id="1st_thu_aim"></td>
                            <td><input type="text" name = "fri_aim" class="form-control"  id="1st_fri_aim"></td>
                            <td><input type="text" name = "sat_aim" class="form-control"  id="1st_sat_aim"></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Aim</td>
                            <td><input type="text" name = "scnd_sunday_aim" class="form-control"  id="2st_sun_aim"></td>
                            <td><input type="text" name = "scnd_monday_aim" class="form-control"  id="2st_mon_aim"></td>
                            <td><input type="text" name = "scnd_tues_aim" class="form-control"  id="2st_tue_aim"></td>
                            <td><Input type="text" name = "scnd_wed_aim" class="form-control"  id="2st_wed_aim"></td>
                            <td><input type="text" name = "scnd_thurs_aim" class="form-control"  id="2st_thu_aim"></td>
                            <td><input type="text" name = "scnd_fri_aim" class="form-control"  id="2st_fri_aim"></td>
                            <td><Input type="text" name = "scnd_sat_aim" class="form-control"  id="2st_sat_aim"></td>
                            
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Details</td>
                            <td><textarea class="form-control" rows="2" id="sun_comment" name="sunday_detail" ></textarea></td>
                            <td><textarea class="form-control" rows="2" id="mon_comment" name="mon_detail" ></textarea></td>
                            <td><textarea class="form-control" rows="2" id="tue_comment" name="tues_detail" ></textarea></td>
                            <td><textarea class="form-control" rows="2" id="wed_comment" name="wed_detail" ></textarea></td>
                            <td><textarea class="form-control" rows="2" id="thu_comment" name="thurs_detail" ></textarea></td>
                            <td><textarea class="form-control" rows="2" id="fri_comment" name="fri_detail" ></textarea></td>
                            <td><textarea class="form-control" rows="2" id="sat_comment" name="sat_detail" ></textarea></td>

                            
                        </tr>
                        <tr>
                            <td class="first_tb_data">Overall Session Detail</td>
                            <!-- <th><span style="float: left;">topic</span><span style="float: right;">Dur</span>
                            <input type="number" name="" placeholder="warm-up" class="form-control">
                            </th> -->
                            <td>
                                <!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                                </div> -->
                           <!--  <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="wed_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="wed_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="wed_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="wed_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="wed_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="wed_rpe" class="new_set_position_2"></span></div> -->


                        <span class="set_topic">Topic With Dur</span>
                        <span class="new_set_position">Warm Up</span>
                            <input type="text" name="1st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="sun_intTextBox" >
                            <span class="new_set_position">Phy</span>
                            <input type="text" name="1st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="sun_phy_part">
                            <span class="new_set_position">Coach</span>
                            <input type="text" name="1st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="sun_coach_part">
                            <span class="new_set_position">Cool</span>
                            <input type="text" name="1st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="sun_cool_down" >

                            <span class="new_set_position">Total</span>
                            <input type="text" name="1st_sun_total" placeholder="Total" class="form-control aa" id="sun_result" readonly>
                            <span class="new_set_position">RPE</span>
                            <input type="text" name="1st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="sun_rpe" >
                        </td>
                        <!-- Monday-->
                        	<td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            	<!-- <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="mon_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="mon_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="mon_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="mon_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="mon_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="mon_rpe" class="new_set_position_2"></span></div> -->
                        <span class="set_topic">Topic With Dur</span>
                            <span class="new_set_position">Warm Up</span>
                            <input type="text" name="2st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="mon_intTextBox" >
                            <span class="new_set_position">Phy</span>
                            <input type="text" name="2st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="mon_phy_part" >
                            <span class="new_set_position">Coach</span>
                            <input type="text" name="2st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="mon_coach_part" >
                            <span class="new_set_position">Cool</span>
                            <input type="text" name="2st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="mon_cool_down" >

                            <span class="new_set_position">Total</span>
                            <input type="text" name="2st_sun_total" placeholder="Total" class="form-control aa" id="mon_result" readonly>
                            <span class="new_set_position">RPE</span>
                            <input type="text" name="2st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="mon_rpe" >
                        </td>
                          <!-- Tuesday-->
                             <td>
                                <!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                                </div> -->
                           <!--  <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="wed_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="wed_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="wed_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="wed_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="wed_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="wed_rpe" class="new_set_position_2"></span></div> -->



                            <span class="set_topic">Topic With Dur</span>
                        <span class="new_set_position">Warm Up</span>
                            <input type="text" name="3st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="tues_intTextBox" >
                            <span class="new_set_position">Phy</span>
                            <input type="text" name="3st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="tues_phy_part" >
                            <span class="new_set_position">Coach</span>
                            <input type="text" name="3st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="tues_coach_part">
                            <span class="new_set_position">Cool</span>
                            <input type="text" name="3st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="tues_cool_down" >

                            <span class="new_set_position">Total</span>
                            <input type="text" name="3st_sun_total" placeholder="Total" class="form-control aa" id="tues_result" readonly>
                            <span class="new_set_position">RPE</span>
                            <input type="text" name="3st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="tue_rpe" >
                        </td>


                        <!--Wednesday -->
                         <td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                           <!--  <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="wed_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="wed_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="wed_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="wed_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="wed_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="wed_rpe" class="new_set_position_2"></span></div> -->


                        <span class="set_topic">Topic With Dur</span>
                        <span class="new_set_position">Warm Up</span>
                            <input type="text" name="4st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="wed_intTextBox" >
                            <span class="new_set_position">Phy</span>
                            <input type="text" name="4st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="wed_phy_part" >
                            <span class="new_set_position">Coach</span>
                            <input type="text" name="4st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="wed_coach_part">
                            <span class="new_set_position">Cool</span>
                            <input type="text" name="4st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="wed_cool_down" >

                            <span class="new_set_position">Total</span>
                            <input type="text" name="4st_sun_total" placeholder="Total" class="form-control aa" id="wed_result" readonly>
                            <span class="new_set_position">RPE</span>
                            <input type="text" name="4st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="wed_rpe" >
                        </td>

                        <td>
                                <!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                                </div> -->
                           <!--  <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="wed_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="wed_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="wed_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="wed_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="wed_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="wed_rpe" class="new_set_position_2"></span></div> -->


                        <span class="set_topic">Topic With Dur</span>
                        <span class="new_set_position">Warm Up</span>
                            <input type="text" name="5st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="thu_intTextBox" >
                            <span class="new_set_position">Phy</span>
                            <input type="text" name="5st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="wed_phy_part" >
                            <span class="new_set_position">Coach</span>
                            <input type="text" name="5st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="thu_coach_part">
                            <span class="new_set_position">Cool</span>
                            <input type="text" name="5st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="thu_cool_down" >

                            <span class="new_set_position">Total</span>
                            <input type="text" name="5st_sun_total" placeholder="Total" class="form-control aa" id="thu_result" readonly>
                            <span class="new_set_position">RPE</span>
                            <input type="text" name="5st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="thu_rpe" >
                        </td>


                        <td>
                                <!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                                </div> -->
                           <!--  <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="wed_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="wed_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="wed_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="wed_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="wed_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="wed_rpe" class="new_set_position_2"></span></div> -->


                        <span class="set_topic">Topic With Dur</span>
                        <span class="new_set_position">Warm Up</span>
                            <input type="text" name="6st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="fri_intTextBox" >
                            <span class="new_set_position">Phy</span>
                            <input type="text" name="6st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="fri_phy_part" >
                            <span class="new_set_position">Coach</span>
                            <input type="text" name="6st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="fri_coach_part">
                            <span class="new_set_position">Cool</span>
                            <input type="text" name="6st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="fri_cool_down" >

                            <span class="new_set_position">Total</span>
                            <input type="text" name="6st_sun_total" placeholder="Total" class="form-control aa" id="fri_result" readonly>
                            <span class="new_set_position">RPE</span>
                            <input type="text" name="6st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="fri_rpe" >
                        </td>

                        <td>
                                <!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                                </div> -->
                           <!--  <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="wed_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="wed_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="wed_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="wed_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="wed_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="wed_rpe" class="new_set_position_2"></span></div> -->


                        <span class="set_topic">Topic With Dur</span>
                        <span class="new_set_position">Warm Up</span>
                            <input type="text" name="7st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="sat_intTextBox" >
                            <span class="new_set_position">Phy</span>
                            <input type="text" name="7st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="sat_phy_part" >
                            <span class="new_set_position">Coach</span>
                            <input type="text" name="7st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="sat_coach_part">
                            <span class="new_set_position">Cool</span>
                            <input type="text" name="7st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="sat_cool_down" >

                            <span class="new_set_position">Total</span>
                            <input type="text" name="7st_sun_total" placeholder="Total" class="form-control aa" id="sat_result" readonly>
                            <span class="new_set_position">RPE</span>
                            <input type="text" name="7st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="sat_rpe" >
                        </td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Tactical</td>
                            <td><input type="text" name="tactical_first" id="tactical_first" class="form-control"></td>
                            <td><input type="text" name="tactical_scnd" id="tactical_scnd" class="form-control"></td>
                            <td><input type="text" name="tactical_third" id="tactical_third" class="form-control"></td>
                            <td><input type="text" name="tactical_fourth" id="tactical_fourth" class="form-control"></td>
                            <td><input type="text" name="tactical_fifth" id="tactical_fifth" class="form-control"></td>
                            <td><input type="text" name="tactical_six" id="tactical_six" class="form-control"></td>
                            <td><input type="text" name="tactical_seven" id="tactical_seven" class="form-control"></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">technical</td>

                            <td><input type="text" name="technical_first" id="technical_first" class="form-control"></td>
                            <td><input type="text"  name="technical_scnd" id="technical_scnd" class="form-control"></td>
                            <td><input type="text" name="technical_third" id="technical_third" class="form-control"></td>
                            <td><input type="text" name="technical_fourth" id="technical_fourth" class="form-control"></td>
                            <td><input type="text"  name="technical_fifth" id="technical_fifth" class="form-control"></td>
                            <td><input type="text" name="technical_six" id="technical_six" class="form-control"></td>
                            <td><input type="text" name="technical_seven" id="technical_seven" class="form-control"></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Mental</td>
                            <td><input type="text" name="mental_first" id="mental_first" class="form-control"></td>
                            <td><input type="text" name="mental_scnd" id="mental_scnd" class="form-control"></td>
                            <td><input type="text" name="mental_third" id="mental_third" class="form-control"></td>
                            <td><input type="text" name="mental_fourth" id="mental_fourth" class="form-control"></td>
                            <td><input type="text" name="mental_fifth" id="mental_fifth" class="form-control"></td>
                            <td><input type="text" name="mental_six" id="mental_six" class="form-control"></td>
                            <td><input type="text" name="mental_seven" id="mental_seven" class="form-control"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <div class="div_holder">
        
        <div id="chartContainer" style="height: 550px; max-width: 920px; margin: 0px auto 100px; display: none; float: left;"></div>
        </div>
        <div class="calculation">
        <div class="title text-center"><h6>Training time</h6></div>
        <div class="time_dur text-center"></div>
        <div class="week_title text-center">Weekly Load</div>
        <div class="week_load text-center"></div>
        </div>
        <div class="footer"><p>Document Preppared By Pro Football Coach.</p>
            <p>0097466053410 Or 0097477080967</p>
            <p>coachredouane@hotmail.com</p>
        </div>
    </div>
            </form>
        </div>
        </section>
         
    </div>
</body>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script src="js/jquery.simplecolorpicker.js"></script>
    <script type='text/javascript' src='js/pdfmake.min.js'></script>
    <script type='text/javascript' src='js/html2canvas.min.js'></script>
     <!-- <script type="text/javascript">
       $('select[name="sun_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
       $('select[name="mon_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
       $('select[name="tues_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('select[name="wed_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});

         $('select[name="thu_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
         $('select[name="fri_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
         $('select[name="sat_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
   </script> -->
   <!--only numbers entered -->
   <script type="text/javascript">
    function setInputFilter(textbox, inputFilter) {
      if(!textbox){
        return false;
      }
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  });
}


// Install input filters.
//Sunday
setInputFilter(document.getElementById("sun_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//monday
setInputFilter(document.getElementById("mon_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//Tuesday
setInputFilter(document.getElementById("tues_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });
//Wednesday
setInputFilter(document.getElementById("wed_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//Thursday
setInputFilter(document.getElementById("thu_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });
//Friday


setInputFilter(document.getElementById("fri_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });


setInputFilter(document.getElementById("sat_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });



   </script>
   <script type="text/javascript">
    // var warm_up=''
    // var phy_part=''
    // var coach_part=''
    // var cool_down=''
       // $('#intTextBox').keyup(function(){
       //      var warm_up=$(this).val();
       // });
       // $('#phy-part').keyup(function(){
       //      var phy_part=$(this).val();
       // });
       // $('#coach-part').keyup(function(){
       //      var coach_part=$(this).val();
       // });
       // $('#cool-down').keyup(function(){
       //      var cool_down=$(this).val();
       //      var total = warm_up+phy_part+coach_part+cool_down;
       //      console.log(total);

       // });
       //console.log(phy_part);
       $('#sun_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#sun_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#sun_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#sun_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //alert('hi');
            //console.log(myVar);
           // alert(sun_warm_up);
           // if(sun_warm_up=='')
           // {
           //  sun_warm_up = $('#sun_intTextBox').val();
           //  aler('hi');
           // }
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#sun_result').val(sun_answer);

        });
       //Monday
       $('#mon_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#mon_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#mon_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#mon_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt($('#mon_intTextBox').val(), 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#mon_result').val(sun_answer);

        });
       //Tuesday
        $('#tues_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#tues_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#tues_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#tues_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#tues_result').val(sun_answer);

        });

       //Wednesday
       $('#wed_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#wed_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#wed_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#wed_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#wed_result').val(sun_answer);

        });

       //Thursday
       $('#thu_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#thu_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#thu_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#thu_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#thu_result').val(sun_answer);

        });

       //Friday

       $('#fri_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#fri_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#fri_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#fri_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#fri_result').val(sun_answer);

        });
       //Sat
       $('#sat_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#sat_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#sat_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#sat_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#sat_result').val(sun_answer);

        });
   </script>

<script>

$("form#plan_data").submit(function() {
    var formData = new FormData($(this)[0]);
    $.post($(this).attr("action"), formData, function(data) {
        //alert(data);
    });
    return false;
});
</script>
<script type="text/javascript">

    // Vishnu Update
$(document).ready(function(){

$('#sun_color').simplecolorpicker({picker: true, theme: 'glyphicons'});
$('#mon_color').simplecolorpicker({picker: true, theme: 'glyphicons'});
$('#tues_color').simplecolorpicker({picker: true, theme: 'glyphicons'});
$('#wed_color').simplecolorpicker({picker: true, theme: 'glyphicons'});
$('#thu_color').simplecolorpicker({picker: true, theme: 'glyphicons'});
$('#fri_color').simplecolorpicker({picker: true, theme: 'glyphicons'});
$('#sat_color').simplecolorpicker({picker: true, theme: 'glyphicons'});


let fld_season_select = document.querySelector("#fld_season_select");
let fld_age_group_select = document.querySelector("#fld_age_group_select");
let fld_week_select = document.querySelector("#fld_week_select");
let fld_month_select = document.querySelector("#fld_month_select");

fld_season_select.addEventListener('change', ChangeUpdate)
fld_age_group_select.addEventListener('change', ChangeUpdate)
fld_week_select.addEventListener('change', ChangeUpdate)
fld_month_select.addEventListener('change', ChangeUpdate)

function ChangeUpdate(event){
  console.log('Debug: Connecting to General_search')  
  if(
    !fld_season_select.value || 
    !fld_age_group_select.value ||
    !fld_week_select.value ||
    !fld_month_select.value
    )
    return false;
  $.ajax({
      type:'POST',
     url:'process/general_search.php',
     dataType: 'json',
     encode: true,
      data:{
        saeson_anc:fld_season_select.value,
        week_group:fld_age_group_select.value,
        week:fld_week_select.value,
        month:fld_month_select.value
      },
    })
    .catch(error=>{
      alert(error);
    })
    .then(function(value){
      let mainData = value[0];
      if(!mainData) return false;

      $(".client_logo").prop("src",mainData.fld_image);
      $('#no-year-datepicker').val(mainData.fld_sun_date);
      $('#monday_date').val(mainData.fld_mon_date);
      $('#tuesday_date').val(mainData.fld_tues_date);
      $('#wed_date').val(mainData.fld_wed_date);
      $('#thursday_date').val(mainData.fld_thu_date);
      $('#friday_date').val(mainData.fld_fri_date);
      $('#sat_date').val(mainData.fld_sat_date);

      $('#sun_color').val(mainData.fld_sun_intensity);
      $('#mon_color').val(mainData.fld_mon_intensity);
      $('#tues_color').val(mainData.fld_tues_intensity);
      $('#wed_color').val(mainData.fld_wed_intensity);
      $('#thu_color').val(mainData.fld_thu_intensity);
      $('#fri_color').val(mainData.fld_fri_intensity);      
      $('#sat_color').val(mainData.fld_sat_intensity);

    });	
}

});


// $("ul li.one > a").live("click",function(){
</script>

<script>
// function fun(sun_total,mon_total,tue_total,wed_total,thu_total,fri_total,sat_total) {
// //Better to construct options first and then pass it as a parameter
// var options = {
// 	animationEnabled: true,
// 	title:{
// 		text: "Performance"   
// 	},
// 	axisY:{
// 		title:"Coal (mn tonnes)"
// 	},
// 	toolTip: {
// 		shared: true,
// 		reversed: true
// 	},
// 	data: [
// 	{
// 		type: "stackedColumn",
// 		name: "SubBituminous and Lignite",
// 		showInLegend: "true",
// 		yValueFormatString: "#,##0mn tonnes",
// 		dataPoints: [
// 			{ y:  sun_total, label: "S" },
// 			{ y:  mon_total, label: "M" },
// 			{ y: tue_total, label: "T" },
// 			{ y: wed_total, label: "W" },
// 			{ y: thu_total, label: "T" },
// 			{ y: fri_total, label: "F" },
// 			{ y: sat_total, label: "S" }
// 		]
// 	}]
// };

// $("#chartContainer").CanvasJSChart(options);
// }
// function fun(sun_total,mon_total,tue_total,wed_total,thu_total,fri_total,sat_total)
// {
// var options = {
//     animationEnabled: true,
//     title:{
//         text: "Coal Reserves of Countries"   
//     },
//     axisY:{
//         title:"Coal (mn tonnes)"
//     },
//     toolTip: {
//         shared: true,
//         reversed: true
//     },
//     data: [
//     {
//         type: "stackedColumn",
//         name: "SubBituminous and Lignite",
//         showInLegend: "true",
//         yValueFormatString: "#,##0mn tonnes",
//         dataPoints: [
//             { y: 135305 , label: "USA" },
//             { y: 107922, label: "Russia" },
//             { y: 52300, label: "China" },
//             { y: 3360, label: "India" },
//             { y: 39900, label: "Australia" },
//             { y: 0, label: "SA" }
//         ]
//     }]
// };

// $("#chartContainer").CanvasJSChart(options);
// }
function fun(sun_total,mon_total,tue_total,wed_total,thu_total,fri_total,sat_total) {
//console.log(sun_total);
//Better to construct options first and then pass it as a parameter
var options = {
    animationEnabled: true,
    title: {
        text: "Performance",                
        fontColor: "Peru"
    },  
    axisY: {
        tickThickness: 0,
        lineThickness: 0,
        valueFormatString: " ",
        gridThickness: 0                    
    },
    axisX: {
        tickThickness: 0,
        lineThickness: 0,
        labelFontSize: 18,
        labelFontColor: "Peru"              
    },
    data: [{
        indexLabelFontSize: 26,
        toolTipContent: "<span style=\"color:#62C9C3\">{indexLabel}:</span> <span style=\"color:#CD853F\"><strong>{y}</strong></span>",
        indexLabelPlacement: "inside",
        indexLabelFontColor: "white",
        indexLabelFontWeight: 600,
        indexLabelFontFamily: "Verdana",
        color: "#62C9C3",
        type: "bar",
        dataPoints: [
            { y: sun_total, label: sun_total, indexLabel: "S" },
            { y: mon_total, label: mon_total, indexLabel: "M" },
            { y: tue_total, label: tue_total, indexLabel: "T" },
            { y: wed_total, label: wed_total, indexLabel: "W" },
            { y: thu_total, label: thu_total, indexLabel: "T" },
            { y: fri_total, label: fri_total, indexLabel: "F" },
            { y: sat_total, label: sat_total, indexLabel: "S" }
            
        ]
    }]
};

$("#chartContainer").CanvasJSChart(options);
}
</script>
<script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>


    
    <script type="text/javascript">
            $("body").on("click", "#download", function () {
                $('.nav-link').css('color', 'red !important');
                // var img=canvas.toDataURL("image/png");
                html2canvas($('.page-wrap')[0], {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("Table.pdf");
                    }
                });
                 //$('.page-wrap').css({color: white});
                
            });
    </script>
    <script type="text/javascript">
       $(function() {
        $("#no-year-datepicker").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#monday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#tuesday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#wed_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#thursday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#friday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
         $("#sat_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#new_date").datepicker({
        dateFormat: "MM",
        changeMonth: true,
        changeYear: false
        });
});
   </script>

</html>


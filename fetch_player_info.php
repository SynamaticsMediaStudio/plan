<?php
include_once("secure.php");
include('dbbridge/top.php');
  error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Player Eveluation Form</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/player_eveluation_form.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>


<body>
    <div class="container">
      <!--Insert Data popup -->
      <?php
    include('side_nav.php');
    ?>
    <input type="text" id="myInput" onkeyup="myFunction1()" placeholder="Search for names" style="float: right; margin: 60px 0 30px 0; width:100%;" class="form-control">
    <select class="form-control" id="category" name="category">
                <option selected="selected">choose</option>
            <?php
            include('dbbridge/top.php');
            $db = new DBManager();
              $play_name_query="SELECT  * FROM player_info";
               $play_name_result=$db->sample($play_name_query);
               //print_r($result);
               foreach ($play_name_result as $key => $play_name_value) {
                # code...
                 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";

                $player_service_provider=$play_name_value['fld_cat'];
                if(in_array($player_service_provider, $player_repeated_data)==0)
                {
                 echo "<option class='dropdown-item ropdown-item month' id='saeson_anc' value=".$play_name_value['fld_cat'].">".$play_name_value['fld_cat']."</option>";
                }
                $player_repeated_data[] = $play_name_value['fld_cat'];
               }
              ?>
                </select>
      <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <form  name="myform" action="delete_data.php" method="POST">
              <p>You Really want to delete your Product?
              </p>
              <input type="hidden" id="delete_id" name="delete_id">
              <input type="hidden" id="" name="check_value" value="1">
              <div class="row">
                <div class="col-md-6">
                  <button type="submit" class="btn btn-warning del_btn" style="margin: 10px 0 0 0;">Delete</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        </div>
      </div>
      <!-- Update-->
      <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          
          <div class="modal-body">
            <form action="update_player_info.php" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Player Number:</label>
                <input type="text" class="form-control player_num" id="player_num" name="player_num">
                <input type="hidden" class="form-control" id="update_id" name="update_id">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <!--  <input type="file" class="file" name="file" id="file" required style="margin: 30px 0 0 0;"> -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Name:</label>
                <input type="text" class="form-control name" id="name" name="name">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Father Name:</label>
                <input type="text" class="form-control f_name" id="f_name" name="f_name">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Nationality:</label>
                <input type="text" class="form-control Nationality" id="Nationality" name="Nationality">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Position:</label>
                <input type="text" class="form-control position" id="position" name="position">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Evaluation:</label>
                <input type="text" class="form-control Evaluation" id="Evaluation" name="Evaluation">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Academy:</label>
                <input type="text" class="form-control Academy" id="Academy" name="Academy">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">National Team:</label>
                <input type="text" class="form-control national_team"  id="national_team" name="national_team">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">contact_number:</label>
                <input type="text" class="form-control contact_number" id="contact_number" name="contact_number">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Father Contact:</label>
                <input type="text" class="form-control father_contact" id="father_contact" name="father_contact">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Foot Info:</label>
                <input type="text" class="form-control foot_info" id="foot_info" name="foot_info">
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Category:</label>
                <input type="text" class="form-control player_cat" id="player_cat" name="player_cat">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Season:</label>
                <input type="text" class="form-control seas" id="seas" name="seas">
            </div>
            <input type="hidden" name="player_image" id="player_image">
        </div>
        <br>
        <input type="submit" name="submit" value="Save" class="btn btn-primary">
    </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        </div>
      </div>


</div>
    <script>
        var cat_id = '<?php echo $cat_id;?>';
      </script>
    <div class="container-fluid">

      <div class="product_record">
  <a href="player_info_detail.php" style="font-size: 23px;"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
<?php
    //echo $cat_id;
    //exit;
    //echo "hi";
    //exit;
    $db = new DBManager();
    //print_r($result);
    //exit;
    $result = $db->get_player_info();
    //print_r($result);
    //exit;
    echo "<h1 style='font-size:8vw;'>Player Information</h1>";
      ?>
      <?php
      //$new_result = mysql_num_rows($result);
      //echo $new_result;
      //exit;
    if(!empty($result[0]))
    {
      //echo "hi";
      //print_r($result);
      //exit;
          //echo "hi";
      ?>
      <div class="table-responsive player_table" id="myTable">
      
        <?php

          ?>
          <!-- <td id="tb_player_num"></td>
          <td id="tb_player_name"></td>
          <td id="tb_father_name"></td>
          <td id="tb_Nationality"></td>
          <td id="tb_position"></td>
          <td id="tb_eval"></td>
          <td id="tb_academy"></td>
          <td id="tb_national_team"></td>
          <td id="tb_contact_num"></td>
          <td id="tb_father_contact"></td>
          <td id="tb_foot_info"></td>
          <td id="tb_player_image"></td>
          <td id="tb_cat"></td>
          <td id="tb_seas"></td> -->
          
          <!-- <td><?php echo $value['player_number'];?></td>
          <td><a><?php echo $value['fld_player_name'];?></a></td>
          <td><?php echo $value['fld_player_father_name'];?></td>
          <td><?php echo $value['fld_player_Nationality'];?></td>
          <td><?php echo $value['fld_player_position'];?></td>
          <td><?php echo $value['fld_player_evaluation'];?></td>
          <td><?php echo $value['fld_player_academy'];?></td>
          <td><?php echo $value['fld_national_team'];?></td>
          <td><?php echo $value['fld_player_contact_number'];?></td>
          <td><?php echo $value['fld_player_father_contact'];?></td>
          <td><?php echo $value['fld_player_foot_info'];?></td>
          <td><?php echo '<img src="'.$value['fld_player_image'].'" class="img-responsive" style="width: 50px;height: 50px;">';?></td>
          <td><?php echo $value['fld_cat'];?></td>
          <td><?php echo $value['fld_season'];?></td> -->
          <!-- <td><button class='btn btn-danger set_btn del' data-toggle='modal' data-target='#delModal' data-id='<?php echo $value['fld_id'];?>'><i class='fa fa-trash'></i></button></td>
          <td><button class='btn btn-primary update set_btn' data-toggle='modal' data-target='#updateModal' data-id='" .$value['fld_id']."'><i class='fa fa-pencil' aria-hidden='true'></i></button></td> -->
        <?php
      //}
      ?>
        <!--Delete Group button -->
        </div>
        
      <?php
      }
      else {
        # code...
        echo "<h1>Not Any Player is Available</h1>";
      }
      //echo $compare;
        //$compare = $final_result;
      //echo $compare;
?>
    </div>
  </div>
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="custom_function/insert.js"></script>
  <script src="custom_function/update.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $(".del").click(function(){
      var del_id = $(this).attr('data-id');
      //alert(del_id);
      //var del_id = $(this).value();
      //console.log(del_id);
      //var c_id = $('#add_product').data('id');
      //alert(c_id)
      //$("#con_id").val(c_id);
      $("#delete_id").val(del_id);
      });
    });
      $(".update").click(function(){
      var updat_id = $(this).attr('data-id');
      //var c_id = $('#add_product').data('id');
      //var player_num=$('#player_num').val();
      var action=1;
      //alert(updat_id);
      $.ajax({
      type:'POST',
     url:'get_player.php',
      data:{
        dataType: 'json',
        encode: true,
        updat_id:updat_id,
        action:action,
      },
    })
      .done(function(value){
      var data = value.split(",");
      $("#player_num").val(data[13]);
      //console.log(data);
      $("#name").val(data[0]);
      $("#f_name").val(data[1]);
      $("#Nationality").val(data[2]);
      $("#position").val(data[3]);
      $("#Evaluation").val(data[4]);
      $("#Academy").val(data[5]);
      $("#national_team").val(data[6]);
      $("#contact_number").val(data[7]);
      $("#father_contact").val(data[8]);
      $("#foot_info").val(data[9]);
      $("#player_cat").val(data[11]);
      $("#seas").val(data[12]);
      $("#player_image").val(data[10]);
      //$(".player_image").prop("src",data[12]);
      });
      // $("#search_id").val(c_id);
      $("#update_id").val(updat_id);
      });
     //alert(id);
  </script>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script src="js/jquery.simplecolorpicker.js"></script>
    <script type="text/javascript">
        $(document).on("click",".shah",function(){
        var del_id = $(this).attr('data-id');
        $('#delete_id').val(del_id);
        });
    </script>
    <script type="text/javascript">
      function myFunction1() {
    // Declare variables 
    //alert('hi');
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
    </script>
    <script>
 // $(document).on("click","#save",function(e){  
 //     //Prevent Instant Click  
 //    e.preventDefault();
 //    // Create an FormData object 
 //        var formData =$("#plan_data").submit(function(e){
 //            return ;
 //        });
 //      //formData[0] contain form data only 
 //      // You can directly make object via using form id but it require all ajax operation inside $("form").submit(<!-- Ajax Here   -->)
 //        var formData = new FormData(formData[0]);    
 //        $.ajax({
 //            url: 'insert_weekly_plan.php',
 //            type: 'POST',
 //            data: formData,
 //            success: function(response) {console.log(response);},
 //            contentType: false,
 //            processData: false,
 //            cache: false
 //        });
 //        return false;
 //            });
 $('#category').change(function(){
    //alert('hi');
    var action=6;
    var category=$(this).val();

    $.ajax({
      type:'POST',
     url:'live_search.php',
      data:{
        dataType: 'json',
        encode: true,
        category:category,
        action:action,
      },
    })
      .done(function(value){
      var data = value.split(",");
     // console.log(data[12]);
      // exit();
      //$(".player_image").prop("src",data[0]);
      // $("#tb_player_name").html(data[0]);
      // $("#tb_father_name").html(data[1]);
      // $("#tb_Nationality").html(data[2]);
      // $("#tb_position").html(data[3]);
      // $("#tb_eval").html(data[4]);
      // $("#tb_academy").html(data[5]);
      // $("#tb_national_team").html(data[6]);
      // $("#tb_contact_num").html(data[7]);
      // $("#tb_father_contact").html(data[8]);
      // $("#tb_foot_info").html(data[9]);
      // $("#tb_cat").html(data[10]);
      // $("#tb_player_image").html(data[12]);
      // $("#tb_seas").html(data[11]);
      $(".table-responsive").html(data[0]);
      //$("#new_row").html(data[1]);
    });
 });
</script>
<script>
   $(document).on("click",".update",function(){
  var id = $(this).closest("tr").find(".new_player_num").text();
  var new_player_name = $(this).closest("tr").find(".new_player_name").text();
  var new_player_f_nam = $(this).closest("tr").find(".new_player_f_name").text();
   var new_player_nationality = $(this).closest("tr").find(".new_player_nationality").text();
   var new_player_position = $(this).closest("tr").find(".new_player_position").text();
   var new_player_evaluation = $(this).closest("tr").find(".new_player_evaluation").text();
    var new_player_academy = $(this).closest("tr").find(".new_player_academy").text();
     var new_player_national_team = $(this).closest("tr").find(".new_player_national_team").text();
      var new_player_con_num = $(this).closest("tr").find(".new_player_con_num").text();

       var new_player_f_num = $(this).closest("tr").find(".new_player_f_num").text();

        var new_player_foot_info = $(this).closest("tr").find(".new_player_foot_info").text();
         var new_player_cat = $(this).closest("tr").find(".new_player_cat").text();
         var new_player_seas = $(this).closest("tr").find(".new_player_seas").text();

         var player_image = $('.player_image').children('img').attr('src');
         var fld_id = $(this).closest("tr").find(".fld_id").text();
  $('#player_num').val(id);
  $('#name').val(new_player_name);
  $('#f_name').val(new_player_f_nam);
  $('#Nationality').val(new_player_nationality);
  $('#position').val(new_player_position);
  $('#Evaluation').val(new_player_evaluation);
  $('#Academy').val(new_player_academy);
  $('#national_team').val(new_player_national_team);
  $('#contact_number').val(new_player_con_num);

  $('#father_contact').val(father_contact);
  $('#foot_info').val(new_player_foot_info);
  $('#father_contact').val(new_player_f_num);
  $('#player_cat').val(new_player_cat);
  $('#player_image').val(player_image);
  $('#seas').val(new_player_seas);
  $('#update_id').val(fld_id);

});
</script>
</body>
</html>
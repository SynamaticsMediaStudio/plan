<?php
include_once("secure.php");
  error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Weekly Plan</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link rel="stylesheet" href="css/login_style.css" media="screen" type="text/css" />
    <link href="css/style.css" rel="stylesheet">
    <link href="css/student_form.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>
<section class="nav_section" style="min-height: 430vh;">
        <div class="container-fluid">
            <form class="form-control" name="form" method="post" enctype="multipart/form-data" id="plan_data" action="insert_weekly_plan.php">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="images/Pro_Football_Coach_Logo.png"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          season
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="season" style="border: none;" required id="season">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Age Group
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="age_group" style="border: none;" id="age_group">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Week
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="week" style="border: none;" required id="week">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Month
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <input type="text" id="new_date" class="new_date form-control" style="border: none;" required  name="month">
        </div>
      </li>
      <li class="nav-item dropdown">
       
        <input type="file" class="file" name="file" id="file" required>
      </li>
    </ul>
  </div>
</nav>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1>Training Objectives</h1>
                <textarea cols="5" rows="3" class="form-control"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="div_hold">
                    <input type="file" class="file set_file" name="file" id="file" required>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="div_hold">
                    <h2>Warm up</h2>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        
                            <label>Duration</label>
                            <input type="text" name="duration" id="duration" class="duration form-control" placeholder="Duration">
                    </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        
                            <label>Duration</label>
                            <input type="text" name="duration" id="duration" class="duration form-control" placeholder="Duration">
                        </div>
                        
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        
                            <label>Duration</label>
                            <input type="text" name="duration" id="duration" class="duration form-control" placeholder="Duration">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        
                            <label>Duration</label>
                            <input type="text" name="duration" id="duration" class="duration form-control" placeholder="Duration">
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</for>
</div>
</section>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
<body>
</body>
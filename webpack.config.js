// webpack.config.js
const VueLoaderPlugin = require('vue-loader/lib/plugin')
var path = require('path');

module.exports = {
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  },
  entry: './src/main.js',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'assets'),
  },  
  resolve: {
    alias: {
      'jquery': 'jquery/src/jquery.js',
      'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
    }
  },  
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      }      
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ]
}
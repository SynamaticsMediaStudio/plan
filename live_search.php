<!DOCTYPE html>
<html lang="en">

<head>
    <title>Player Eveluation Form</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/player_eveluation_form.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>


<body>
<?php
include('dbbridge/top.php');
error_reporting(0);
$db = new DBManager();
$player_num = $_POST['player_num'];
if(isset($_POST['action']) && $_POST['action']==1)
{
	$query="SELECT * FROM player_info WHERE player_number='".$player_num."'";
	// print_r($query);
	// exit();
	$result =$db->sample($query);
	// print_r($result);
	// exit();
	foreach ($result as $key => $value) {
		# code...
		echo $value['fld_player_name'].','.$value['fld_player_father_name'].','.$value['fld_player_Nationality'].','.$value['fld_player_position'].','.$value['fld_player_evaluation'].','.$value['fld_player_academy'].','.$value['fld_national_team'].','.$value['fld_player_contact_number'].','.$value['fld_player_father_contact'].','.$value['fld_player_foot_info'].','.$value['fld_player_image'];
	}
}
if(isset($_POST['action']) && $_POST['action']==2)
{
	$player_num = $_POST['player_num'];
	$query="SELECT * FROM player_info WHERE player_number=".$player_num."";
	$result =$db->sample($query);
	foreach ($result as $key => $value) {
		# code...
		echo $value['fld_player_image'];
	}
}

if(isset($_POST['action']) && $_POST['action']==3)
{
	$player_num = $_POST['player_num'];
	$query="SELECT * FROM player_evaluation WHERE player_number=".$player_num."";
	$result =$db->sample($query);
	//print_r($result);
	foreach ($result as $key => $value) {
		//echo $value['fld_tech_eval'];
		//print_r($value['fld_tactical']);
		$fld_tactical = explode(',',$value['fld_tactical']);
		$fld_tac_arabic = explode(',',$value['fld_tac_arabic']);
		$fld_tac_eval = explode(',',$value['fld_tac_eval']);
		$fld_technical = explode(',',$value['fld_technical']);

		$fld_tech_arabic = explode(',',$value['fld_tech_arabic']);

		$fld_tech_eval = explode(',',$value['fld_tech_eval']);


		$fld_physical = explode(',',$value['fld_physical']);
		$fld_phy_arabic = explode(',',$value['fld_phy_arabic']);
		$fld_phy_eval = explode(',',$value['fld_phy_eval']);

		$fld_psychosocial = explode(',',$value['fld_psychosocial']);
		$fld_psy_arabic = explode(',',$value['fld_psy_arabic']);
		$fld_psy_eval = explode(',',$value['fld_psy_eval']);
		//print_r($fld_tactical);
		// exit();
		# code...
		 $var='<option value='.$value['fld_player_name'].'>'.$value['fld_player_name'].'</option>'.','.'<option value='.$value['fld_player_father_name'].'>'.$value['fld_player_father_name'].'</option>'.','.'<option value='.$value['fld_player_Nationality'].'>'.$value['fld_player_Nationality'].'</option>'.','.'<option value='.$value['fld_player_position'].'>'.$value['fld_player_position'].'</option>'.','.'<option value='.$value['fld_player_evaluation'].'>'.$value['fld_player_evaluation'].'</option>'.','.'<option value='.$value['fld_player_academy'].'>'.$value['fld_player_academy'].'</option>'.','.'<option value='.$value['fld_national_team'].'>'.$value['fld_national_team'].'</option>'.','.'<option value='.$value['fld_player_contact_number'].'>'.$value['fld_player_contact_number'].'</option>'.','.'<option value='.$value['fld_player_father_contact'].'>'.$value['fld_player_father_contact'].'</option>'.','.'<option value='.$value['fld_player_foot_info'].'>'.$value['fld_player_foot_info'].'</option>'.','.'<option value='.$value['fld_cat'].'>'.$value['fld_cat'].'</option>'.','.'<option value='.$value['fld_season'].'>'.$value['fld_season'].'</option>'.','.$value['fld_player_image'].',';
		 foreach ($fld_tactical as $key => $fld_tactical_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_tactical_val.'" class="form-control set_dynamic_text" name="tac_eng_title">';
		 } 
		  foreach ($fld_tac_arabic as $key => $fld_tac_arabic_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_tac_arabic_val.'" class="form-control set_dynamic_text" name="tac_arabic">';
		 }
		 foreach ($fld_tac_eval as $key => $fld_tac_eval_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_tac_eval_val.'" class="form-control set_dynamic_text" name="tac_eval">';
		 }
		 //Technical part
		 $var=$var.',';
		 foreach ($fld_technical as $key => $fld_technical_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_technical_val.'" class="form-control set_dynamic_text" name="tech_eng_title">';
		 }
		 foreach ($fld_tech_arabic as $key => $fld_tech_arabic_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_tech_arabic_val.'" class="form-control set_dynamic_text" name="tech_arabic">';
		 }
		 foreach ($fld_tech_eval as $key => $fld_tech_eval_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_tech_eval_val.'" class="form-control set_dynamic_text" name="tech_eval">';
		 }
		 //Physical Value
		  $var=$var.',';
		 foreach ($fld_physical as $key => $fld_physical_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_physical_val.'" class="form-control set_dynamic_text" name="phy_eng_title">';
		 }

		 foreach ($fld_phy_arabic as $key => $fld_phy_arabic_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_phy_arabic_val.'"class="form-control set_dynamic_text" name="phy_arabic">';
		 }

		 foreach ($fld_phy_eval as $key => $fld_phy_eval_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_phy_eval_val.'" class="form-control set_dynamic_text" name="phy_eval">';
		 }
		 //Psy
		 $var=$var.',';
		 foreach ($fld_psychosocial as $key => $fld_psychosocial_val) {
		 	# code...
		 	$var=$var.'<input type="text" value="'.$fld_psychosocial_val.'"class="form-control set_dynamic_text" name="psy_eng_title">';

		 }

		 foreach ($fld_psy_arabic as $key => $fld_psy_arabic_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_psy_arabic_val.'"class="form-control set_dynamic_text" name="psy_arabic">';
		 }

		 foreach ($fld_psy_eval as $key => $fld_psy_eval_val) {
		 	# code...

		 	$var=$var.'<input type="text" value="'.$fld_psy_eval_val.'" class="form-control set_dynamic_text" name="psy_eval">';
		 }
		 $var=$var.',';
		 $var=$var.'<textarea class="form-control set_dynamic_text" name="remarks">'.$value['fld_remarks'].'</textarea>';

		 $var=$var.',';
		 $var=$var.'<option value='.$value['fld_id'].'>'.$value['fld_player_sel'].'</option>';
		 echo $var;
	}
}
if(isset($_POST['action']) && $_POST['action']==4)
{
	$player_num = $_POST['player_num'];
	$query="SELECT * FROM player_info WHERE player_number=".$player_num."";
	$result =$db->sample($query);
	foreach ($result as $key => $value) {
		# code...
	echo '<option value='.$value['fld_player_name'].'>'.$value['fld_player_name'].'</option>'.','.'<option value='.$value['fld_player_father_name'].'>'.$value['fld_player_father_name'].'</option>'.','.'<option value='.$value['fld_player_Nationality'].'>'.$value['fld_player_Nationality'].'</option>'.','.'<option value='.$value['fld_player_position'].'>'.$value['fld_player_position'].'</option>'.','.'<option value='.$value['fld_player_evaluation'].'>'.$value['fld_player_evaluation'].'</option>'.','.'<option value='.$value['fld_player_academy'].'>'.$value['fld_player_academy'].'</option>'.','.'<option value='.$value['fld_national_team'].'>'.$value['fld_national_team'].'</option>'.','.'<option value='.$value['fld_player_contact_number'].'>'.$value['fld_player_contact_number'].'</option>'.','.'<option value='.$value['fld_player_father_contact'].'>'.$value['fld_player_father_contact'].'</option>'.','.'<option value='.$value['fld_player_foot_info'].'>'.$value['fld_player_foot_info'].'</option>'.','.'<option value='.$value['fld_cat'].'>'.$value['fld_cat'].'</option>'.','.'<option value='.$value['fld_season'].'>'.$value['fld_season'].'</option>'.','.$value['fld_player_image'].',';
	}

}
if(isset($_POST['action']) && $_POST['action']==5)
{
	$query="SELECT * FROM player_info WHERE player_number='".$player_num."'";
	// print_r($query);
	// exit();
	$result =$db->sample($query);
	// print_r($result);
	// exit();
	foreach ($result as $key => $value) {
		# code...
		echo $value['fld_player_name'].','.$value['fld_player_father_name'].','.$value['fld_player_Nationality'].','.$value['fld_player_position'].','.$value['fld_player_evaluation'].','.$value['fld_player_academy'].','.$value['fld_national_team'].','.$value['fld_player_contact_number'].','.$value['fld_player_father_contact'].','.$value['fld_player_foot_info'].','.$value['fld_player_image'];
	}
}
if(isset($_POST['action']) && $_POST['action']==6)
{
	$search_cat=$_POST['category'];
	$query="SELECT * FROM player_info WHERE fld_cat='".$search_cat."'";
	// print_r($query);
	// exit();

	$result =$db->sample($query);
	// print_r($result);
	// exit();
	?>
	<table class="table table-bordered">
        <tr>
          <th>Player Number</th>
          <th>Player Name</th>
          <th>Father Name</th>
          <th>Nationality</th>
          <th>Position</th>
          <th>Evaluation</th>
          <th>Academy</th>
          <th>National Team</th>
          <th>Contact Number</th>
          <th>Father Contact</th>
          <th>Foot Info</th>
          <th>Player Image</th>
          <th>Category</th>
          <th>Season</th>
          <th>Delete</th>
          <th>Update</th>
        </tr>
        <?php
	foreach ($result as $key => $value) {
		# code...
		// echo $value['fld_player_name'].','.$value['fld_player_father_name'].','.$value['fld_player_Nationality'].','.$value['fld_player_position'].','.$value['fld_player_evaluation'].','.$value['fld_player_academy'].','.$value['fld_national_team'].','.$value['fld_player_contact_number'].','.$value['fld_player_father_contact'].','.$value['fld_player_foot_info'].','.$value['fld_cat'].','.$value['fld_season'].','.$value['fld_player_image'].','.$value['player_number'];
		echo "<tr>
		<td class='fld_id d-none'>".$value['fld_id']."</td>
				<td class='new_player_num'>".$value['player_number']."</td>
				<td class='new_player_name'>".$value['fld_player_name']."</td>
				<td class='new_player_f_name'>".$value['fld_player_father_name']."</td>
				<td class='new_player_nationality'>".$value['fld_player_Nationality']."</td>
				<td class='new_player_position'>".$value['fld_player_position']."</td>
				<td class='new_player_evaluation'>".$value['fld_player_evaluation']."</td>
				<td class='new_player_academy'>".$value['fld_player_academy']."</td>
				<td class='new_player_national_team'>".$value['fld_national_team']."</td>
				<td class='new_player_con_num'>".$value['fld_player_contact_number']."</td>
				<td class='new_player_f_num'>".$value['fld_player_father_contact']."</td>
				<td class='new_player_foot_info'>".$value['fld_player_foot_info']."</td>
				<td class='player_image'><img src=".$value['fld_player_image']." class='img-responsive new_image' style='width:150px; height:100px;'></td>
				<td class='new_player_seas'>".$value['fld_season']."</td>
				<td class='new_player_cat'>".$search_cat."</td>
				<td><button class='btn btn-danger set_btn shah' data-toggle='modal' data-target='#delModal' data-id='".$value['fld_id']."'><i class='fa fa-trash'></i></button></td>
				<td><button class='btn btn-primary update set_btn' data-toggle='modal' data-target='#updateModal' data-id='" .$value['fld_id']."' id='update_id'><i class='fa fa-pencil'aria-hidden='true'></i></button></td>

		</tr>";
	}
	?>
	</table>
	<?php
}
if(isset($_POST['action']) && $_POST['action']==7)
{
	$del_id=$_POST['del_id'];
	$query ="DELETE FROM player_info Where player_number=".$del_id."";
	$result=$db->sample($query);
	if($result)
	{
		echo "<script>alert('data is deleted')</script>";
	}
	else{
		echo "<script>alert('There is an error')</script>";
	}

}
?>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
</body>
</html>
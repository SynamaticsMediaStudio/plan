import Vue from 'vue';
import App from "./App.vue";
import './scss/main.scss';
import axios from './plugins/axios';
import DatePicker from 'vue2-datepicker';

const files = require.context('./components/', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.use(DatePicker);
Vue.prototype.$http = axios;
Vue.prototype.$moment = require('moment');
const instance = new Vue({
    el:"#app",
    render: h => h(App)
});
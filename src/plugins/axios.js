import Axios from 'axios';

export default Axios.create({
    baseURL:"/api/",
    headers:{
        "Accept": "application/json",
        'Content-Type': 'multipart/form-data'
    }
})
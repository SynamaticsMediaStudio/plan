<?php 
namespace App\Controllers;
use App\Week;
use Carbon\Carbon;
use App\Request;

class IndexController  
{
    public function index()
    {
        $request = Request::request();
        return response($request,true);
    }
    public function categories()
    {
        $week   = new Week;
        $categories = $week->getCategories();
        return response($categories,true);
    }
    public function category($slug)
    {
        $week       = new Week;
        $category   = $week->getCategory($slug);
        return response($category,true);
    }
    public function week($id)
    {
        $week       = new Week;
        $weekData   = $week->getWeek($id);
        return response($weekData,true);
    }
    public function createCategory()
    {
        $request    = Request::request();
        $files      = Request::files();
        $week       = new Week;
        $validate   = Request::validate(
            [
                "age_group"=>"required",
                "start_date"=>"required",
                "logo"=>"nullable|file",
            ]
        );
        
        if($validate->status !== true) return response($validate);

        if(isset($files->logo)){
            $fileFolder = "uploads/".uniqid().".".$files->logo->extension;
            move_uploaded_file($files->logo->tmp_name, $fileFolder);
            $request->logo  =    $fileFolder;
        }

        $options    = [
            "age_group" => $request->age_group,
            "start_date" => Carbon::parse($request->start_date)->format('Y-m-d'),
            "logo" => $request->logo ?? null,
        ];

        $createCategory   = $week->createCategory($options);
        if(!$createCategory && $request->logo):
            \unlink($request->logo);
        endif;
        return response($createCategory);
        
    }
    public function updateWeek($id)
    {
        $week       = new Week;
        $request    = Request::request();
        // return response($request);
        // return var_dump((array) $request);
        $updateWeek   = $week->updateWeek($id,(array) $request);
        return response($updateWeek);
        
    }
}

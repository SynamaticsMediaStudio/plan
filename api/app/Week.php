<?php 
namespace App;
use PDO;
use Carbon\Carbon;
class Week extends Database 
{
    private $database;
    public function __construct()
    {
        $this->database = new Database;
    }
    function getCategories(){
        $result = $this->database->query('SELECT DISTINCT age_group FROM week');
        $result->execute();
		$result->setFetchMode(PDO::FETCH_OBJ);
        return $result->fetchAll();
    }
    function getCategory($name){
        $result = $this->database->query("SELECT * FROM week WHERE age_group='$name'");
		$result->execute();
		$result->setFetchMode(PDO::FETCH_OBJ);
		return $result->fetchAll();
    }
    function getCategoryId($id){
        $result = $this->database->query("SELECT * FROM week WHERE id='$id'");
		$result->execute();
		$result->setFetchMode(PDO::FETCH_OBJ);
		return $result->fetch();
    }
    function getWeek($id){
        $result = $this->database->query("SELECT * FROM weekly WHERE week_id='$id'");
		$result->execute();
		$result->setFetchMode(PDO::FETCH_OBJ);
		return $result->fetchAll();
    }
    function getWeeklyItem($id){
        $result = $this->database->query("SELECT * FROM weekly WHERE id='$id'");
		$result->execute();
		$result->setFetchMode(PDO::FETCH_OBJ);
		return $result->fetch();
    }
    function checkExists($category,$startDate){
        $app = false;
        $dateFirst   = Carbon::parse($startDate);
        for ($i=0; $i < 7; $i++) { 
            $dates    = Carbon::parse($dateFirst)->addDays($i)->format('Y-m-d');
            $result = $this->database->query("SELECT * FROM week WHERE age_group='$category' AND start_date='$dates'");
            $result->execute();
            if($result->rowCount() >= 1)
                $app = true;
        }
        return $app;
    }
    function createCategory(Array $items){
        if(empty($items['age_group']) || empty($items['start_date'])){
            return false;
        }
        else{
            
            if(!$this->checkExists($items['age_group'],$items['start_date'])):
                $conn   = $this->database->connect();
                $stmt   = $conn->prepare("INSERT INTO week (age_group,start_date,logo) VALUES(:age_group,:start_date,:logo)");
                $stmt->execute($items);
                $this->createWeek($conn->lastInsertId());
                return true;
            else:
                return false;
            endif;
        }
    }
    private function createWeek($week_id){

        if(empty($week_id)){
            return false;
        }
        else{
            $exists     = $this->getCategoryId($week_id);
            $dateFirst   = Carbon::parse($exists->start_date);
            $stmt = $this->database->prepare("INSERT INTO weekly (week_id,week_date) VALUES(:week_id,:week_date)");
            for ($i=0; $i < 7; $i++) { 
                $dates    = Carbon::parse($exists->start_date)->addDays($i)->format('Y-m-d');
                $stmt->execute(['week_id'=>$exists->id,'week_date'=>$dates]);
            }
            return true;
        }
    }
    function updateWeek($week_id,Array $data){
        if(empty($week_id)) return false;
        $exists     = $this->getWeeklyItem($week_id);
        if(empty($exists)) return false;
        $stmt = $this->database->prepare(
            "UPDATE weekly
                SET 
                intensity=:intensity, 
                aim=:aim, 
                details=:details, 
                warm_up=:warm_up, 
                phy=:phy, 
                coach=:coach, 
                cool=:cool, 
                total=:total, 
                rpe=:rpe, 
                tactical=:tactical, 
                technical=:technical, 
                mental=:mental 
                WHERE id='$week_id'"
            );
        $data   = (object) $data;
        $exe    =           $stmt->execute([
            'intensity'=>$data->intensity ?? null,
            'aim'=>$data->aim ?? null,
            'details'=>$data->details ?? null,
            'warm_up'=>$data->warm_up ?? null,
            'phy'=>$data->phy ?? null,
            'coach'=>$data->coach ?? null,
            'cool'=>$data->cool ?? null,
            'total'=>$data->total ?? null,
            'rpe'=>$data->rpe ?? null,
            'tactical'=>$data->tactical ?? null,
            'technical'=>$data->technical ?? null,
            'mental'=>$data->mental ?? null,
        ]);

        return $exe;
    }
}

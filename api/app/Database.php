<?php 
namespace App;
use PDO,PDOException;
/**
 * Database Class
 */
class Database  extends PDO
{
	private $cred	=	[
			'host'=>"localhost",
			'dbname' => "plan", 
			'user' => "root",
			"password" => ""
	];
	public function __construct()
	{
		return $this->connect();
	}
	public function connect()
	{
		$connection = (object) $this->cred;
		try {
			$conn = new PDO("mysql:host=$connection->host;dbname=$connection->dbname", $connection->user, $connection->password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		}
		catch(PDOException $e)
		{
			echo "Connection failed: " . $e->getMessage();
		}		
	}
	public function query($query)
	{
		$stmt = $this->connect()->prepare($query);
		return $stmt;
	}
	public function prepare($query,$options=[])
	{
		$stmt = $this->connect()->prepare($query,$options);
		return $stmt;
	}
}

<?php 

function response($data,$pretty = false)
{
    header("Content-Type:application/json");
    echo json_encode($data,$pretty?JSON_PRETTY_PRINT:null);
}
function findReplace($string, $find, $replace)
{
    if (preg_match("/[a-zA-Z\_]+/", $find)) {
        return (string) preg_replace("/\{\{(\s+)?($find)(\s+)?\}\}/", $replace, $string);
    } else {
        throw new \Exception("Find statement must match regex pattern: /[a-zA-Z]+/");
    }
}
<?php 
namespace App;

class Request
{
    public static function request()
    {
        $request    = (object) $_REQUEST;
        switch ($_SERVER["REQUEST_METHOD"]) {
            case 'POST':
                $request    = (object) $_POST;
                break;
            case 'PUT':
                $_PUT   =   json_decode(file_get_contents("php://input"));
                $request = (object) $_PUT;
                break;
            default:
                $request    = (object) $_REQUEST;
                break;
        }
        return $request;
    }
    public static function files()
    {
        $request    = (object) $_FILES;
        foreach ($request as $key => $value) {
            $value            = (object) $value;
            $value->extension = strtolower(pathinfo($value->name,PATHINFO_EXTENSION));
            $request->$key    = $value;
        }
        return $request;
    }
    public static function validate(Array $items)
    {
        $request    = $_REQUEST;
        $files      = $_FILES;
        $status      = ['status'=>true,'errors'=>[]];
        foreach ($items as $key => $value) {
            $reqirements = explode("|",$value);
            $type        = array_key_exists('file',$reqirements) ? $files:$request;
            
            if(array_key_exists('required',$reqirements)){
                if(empty($type[$key])):
                    $status['status'] = false;
                    $status['errors'][$key] = 'Not found';
                endif;
            }
        }
        return (object) $status;
    }
}

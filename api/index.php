<?php include "./vendor/autoload.php";


use Pecee\SimpleRouter\SimpleRouter;

// $categories = $week->getCategories();
// $category   = $week->getCategory('U10');
// $weekData   = $week->getWeek('1');
// $getCategoryId   = $week->getCategoryId('1');
// $createCategory   = $week->createCategory([
//     'age_group'=>"U10",
//     'start_date'=>"2020-10-20",
//     'logo'=>"uploads/U20/one.png",
// ]);
// $updateWeek   = $week->updateWeek(2,['intensity'=>"#9e9e9e"]);
// var_dump($updateWeek);
SimpleRouter::setDefaultNamespace('\App\Controllers');
SimpleRouter::get('/api/', "IndexController@index");
SimpleRouter::get('/api/categories', "IndexController@categories");
SimpleRouter::post('/api/category', "IndexController@createCategory");
SimpleRouter::get('/api/category/{id}', "IndexController@category");
SimpleRouter::get('/api/week/{id}', "IndexController@week");
SimpleRouter::put('/api/week/{id}/update', "IndexController@updateWeek");
SimpleRouter::start();

<?php
include_once("secure.php");
  error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Pro Football Coach</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>


<body  text="green" style="height: 342vh;">
<div  class="page-wrap" style="overflow: hidden;">
    <section class="nav_section">
        <div class="container-fluid">
            <form class="form-control" name="form" method="post" enctype="multipart/form-data" id="plan_data" action="insert_weekly_plan.php">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="images/Pro_Football_Coach_Logo.png"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown .saeson">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          season
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <!-- <input type="text" class="form-control" name="season" style="border: none;" required id="season"> -->
	          <?php
				include('dbbridge/top.php');
				$db = new DBManager();
	          $query="SELECT * FROM weekly_plan";
	           $result=$db->sample($query);
	           //print_r($result);
	           foreach ($result as $key => $value) {
	           	# code...
	           	 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";
                $service_provider=$value['fld_season'];
                if(in_array($service_provider, $repeated_data)==0)
                {
	           	 echo "<a class='dropdown-item saeson_anc' id='saeson_anc' name='saeson_anc_name'>".$value['fld_season']."</a>";
                }
                $repeated_data[] = $value['fld_season'];
	           }
	          ?>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Age Group
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <?php
			include('dbbridge/top.php');
			$db = new DBManager();
	          $query="SELECT * FROM weekly_plan";
	           $result=$db->sample($query);
	           //print_r($result);
	           foreach ($result as $key => $value) {
	           	# code...
	           	 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";
                $service_provider=$value['fld_age_group'];
                if(in_array($service_provider, $repeated_data)==0)
                {
                 echo "<a class='dropdown-item week_group' id='saeson_anc' name='saeson_anc_name'>".$value['fld_age_group']."</a>";
                }
                $repeated_data[] = $value['fld_age_group'];
               }
	          ?>
        </div>
      </li>
      <li class="nav-item dropdown Week_cls">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Week
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <?php
			include('dbbridge/top.php');
			$db = new DBManager();
	          $query="SELECT * FROM weekly_plan";
	           $result=$db->sample($query);
	           //print_r($result);
	           foreach ($result as $key => $value) {
	           	# code...
	           	 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";
                $service_provider=$value['fld_week'];
                if(in_array($service_provider, $repeated_data)==0)
                {
                 echo "<a class='dropdown-item week' id='saeson_anc'name='saeson_anc_name'>".$value['fld_week']."</a>";
                }
                $repeated_data[] = $value['fld_week'];

	           	 // echo "<a class='dropdown-item week'>".$value['fld_week']."</a>";
	           }
	          ?>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Month
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <?php
			include('dbbridge/top.php');
			$db = new DBManager();
	          $query="SELECT * FROM weekly_plan";
	           $result=$db->sample($query);
	           //print_r($result);
	           foreach ($result as $key => $value) {
	           	# code...
	           	 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";

                $service_provider=$value['fld_month'];
                if(in_array($service_provider, $repeated_data)==0)
                {
                 echo "<span class='dropdown-item ropdown-item month' id='saeson_anc'name='saeson_anc_name'>".$value['fld_month']."</span>";
                }
                $repeated_data[] = $value['fld_month'];
	           }
	          ?>
        </div>
      </li>
      <li class="nav-item dropdown">
       
        <img class="client_logo">
      </li>
    </ul>
  </div>
</nav>
<button type="button" id="download" class="btn btn-info" style="float: right;margin: 0 0 10px 0; margin: 40px 0 10px 0;">Download</button>
            <h1 align="top">Weekly Plan</h1>
            
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Day</th>
                            <th scope="col">Sunday</th>
                            <th scope="col">Monday</th>
                            <th scope="col">Tuesday</th>
                            <th scope="col">Wednesday</th>
                            <th scope="col">Thursday</th>
                            <th scope="col">Friday</th>
                            <th scope="col">Saturday</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="first_tb_data">Date</td>
                            <td id="no-year-datepicker"><!-- <input type="text" id="no-year-datepicker" class="set_date sun_date" required name="sun_date" disabled=""> --></td>
                            <td id="monday_date"></td>
                            <td id="tuesday_date"><!-- <input type="text" id="tuesday_date" class="set_date" required name="tuesday_date"> --></td>
                            <td id="wed_date"><!-- <input type="text" id="wed_date" class="set_date" required name="wed_date"> --></td>
                            <td id="thursday_date"><!-- <input type="text" id="thursday_date" class="set_date" required name="thursday_date"> --></td>
                            <td id="friday_date"><!-- <input type="text" id="friday_date" class="set_date" required name="friday_date"> --></td>
                            <td id="sat_date"><!-- <input type="text" id="sat_date" class="set_date" required name="sat_date"> --></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Intensity</td>
                            <td class="color_align">
	                            <select name="sun_color" required id="sun_color" class="new_sun_color">
	                            </select>
	                        </td>
                            <td class="color_align">
                            <select name="mon_color" required id="mon_color">
                                
                            </select></td>
                            <td class="color_align">
                            <select name="tues_color" required id="tues_color">
                                
                            </select></td>
                            <td class="color_align">
                            <select name="wed_color" required id="wed_color">
                               
                            </select></td>
                            <td class="color_align">
                            <select name="thu_color" required id="thu_color">
                              
                            </select></td>
                            <td class="color_align">
                            <select name="fri_color" required id="fri_color">
                               
                            </select></td>
                            <td class="color_align">
                            <select name="sat_color" required id="sat_color">
                            
                            </select></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Aim</td>
                            <td id="1st_sun_aim" style="font-weight: 600;"><!-- <Input type="text" name = "sunday_aim" class="form-control" reqired id="1st_sun_aim"> --></td>
                            <td id="1st_mon_aim" style="font-weight: 600;"><!-- Input type="text" name = "monday_aim" class="form-control" required id="1st_mon_aim"> --></td>
                            <td id="1st_tue_aim" style="font-weight: 600;"><!-- <Input type="text" name = "tues_aim" class="form-control" required id="1st_tue_aim"> --></td>
                            <td id="1st_wed_aim" style="font-weight: 600;"><!-- <Input type="text" name = "wed_aim" class="form-control" required id="1st_wed_aim"> --></td>
                            <td id="1st_thu_aim" style="font-weight: 600;"><!-- <Input type="text" name = "thurs_aim" class="form-control" required id="1st_thu_aim"> --></td>
                            <td id="1st_fri_aim" style="font-weight: 600;"><!-- <Input type="text" name = "fri_aim" class="form-control" required id="1st_fri_aim"> --></td>
                            <td id="1st_sat_aim" style="font-weight: 600;"><!-- <Input type="text" name = "sat_aim" class="form-control" required id="1st_sat_aim"> --></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Aim</td>
                            <td id="2st_sun_aim"><!-- <Input type="text" name = "scnd_sunday_aim" class="form-control" required id="2st_sun_aim"> --></td>
                            <td id="2st_mon_aim"><!-- <Input type="text" name = "scnd_monday_aim" class="form-control" required id="2st_mon_aim"> --></td>
                            <td id="2st_tue_aim"><!-- <Input type="text" name = "scnd_tues_aim" class="form-control" required id="2st_tue_aim"> --></td>
                            <td id="2st_wed_aim"><!-- <Input type="text" name = "scnd_wed_aim" class="form-control" required id="2st_wed_aim"> --></td>
                            <td id="2st_thu_aim"><!-- <Input type="text" name = "scnd_thurs_aim" class="form-control" required id="2st_thu_aim"> --></td>
                            <td id="2st_fri_aim"><!-- <Input type="text" name = "scnd_fri_aim" class="form-control" required id="2st_fri_aim"> --></td>
                            <td id="2st_sat_aim"><!-- <Input type="text" name = "scnd_sat_aim" class="form-control" required id="2st_sat_aim">  --></td>
                            
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Details</td>
                            <td id="sun_comment"><!-- <textarea class="form-control" rows="2" id="sun_comment" name="sunday_detail" required></textarea> --></td>
                            <td id="mon_comment"><!-- <textarea class="form-control" rows="2" id="mon_comment" name="mon_detail" required></textarea> --></td>
                            <td  id="tue_comment"><!-- <textarea class="form-control" rows="2" id="tue_comment" name="tues_detail" required></textarea> --></td>
                            <td id="wed_comment"><!-- <textarea class="form-control" rows="2" id="wed_comment" name="wed_detail" required></textarea> --></td>
                            <td id="thu_comment"><!-- <textarea class="form-control" rows="2" id="thu_comment" name="thurs_detail" required></textarea> --></td>
                            <td id="fri_comment"><!-- <textarea class="form-control" rows="2" id="fri_comment" name="fri_detail" required></textarea> --></td>
                            <td id="sat_comment"><!-- <textarea class="form-control" rows="2" id="sat_comment" name="sat_detail" required></textarea> --></td>

                            
                        </tr>
                        <tr>
                            <td class="first_tb_data">Overall Session Detail</td>
                            <!-- <th><span style="float: left;">topic</span><span style="float: right;">Dur</span>
                            <input type="number" name="" placeholder="warm-up" class="form-control">
                            </th> -->
                            <td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            <span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="sun_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="sun_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="sun_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="sun_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="sun_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="sun_rpe" class="new_set_position_2"></span></div>

                            <!-- <input type="text" name="1st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="sun_intTextBox" required> -->
                            <!-- <input type="text" name="1st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="sun_phy_part" required>
                            <input type="text" name="1st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="sun_coach_part" required>
                            <input type="text" name="1st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="sun_cool_down" required>
                            <input type="text" name="1st_sun_total" placeholder="Total" class="form-control aa" id="sun_result" required>
                            <input type="text" name="1st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="sun_rpe" required> -->
                        </td>
                        <!-- Monday-->
                        	<td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            	<span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="mon_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="mon_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="mon_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="mon_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="mon_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="mon_rpe" class="new_set_position_2"></span></div>
                        <!-- <span style="float: left; font-size: 14px;">Topic With Dur</span>
                            <input type="text" name="2st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="mon_intTextBox" required>
                            <input type="text" name="2st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="mon_phy_part" required>
                            <input type="text" name="2st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="mon_coach_part" required>
                            <input type="text" name="2st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="mon_cool_down" required>
                            <input type="text" name="2st_sun_total" placeholder="Total" class="form-control aa" id="mon_result" required>
                            <input type="text" name="2st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="mon_rpe" required> -->
                        </td>
                          <!-- Tuesday-->
                            <td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            	<span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="tues_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="tues_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="tues_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="tues_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="tues_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="tue_rpe" class="new_set_position_2"></span></div>
                        <!-- <span style="float: left; font-size: 14px;">Topic With Dur</span>
                            <input type="text" name="2st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="mon_intTextBox" required>
                            <input type="text" name="2st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="mon_phy_part" required>
                            <input type="text" name="2st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="mon_coach_part" required>
                            <input type="text" name="2st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="mon_cool_down" required>
                            <input type="text" name="2st_sun_total" placeholder="Total" class="form-control aa" id="mon_result" required>
                            <input type="text" name="2st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="mon_rpe" required> -->
                        </td>


                        <!--Wednesday -->
                         <td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            	<span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="wed_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="wed_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="wed_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="wed_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="wed_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="wed_rpe" class="new_set_position_2"></span></div>
                        </td>

                        <td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            	<span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="thu_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="thu_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="thu_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="thu_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="thu_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="thu_rpe" class="new_set_position_2"></span></div>
                        </td>


                        <td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            	<span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="fri_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="fri_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="fri_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="fri_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="fri_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="fri_rpe" class="new_set_position_2"></span></div>
                        </td>

                        <td>
                            	<!-- <div><span class="set_position">Topic </span><span  class="set_position_2">Dur</span>
                            	</div> -->
                            	<span class="set_topic">Topic With Dur</span>
                            <div class ="head_div"><span class="new_set_position">Warm-up</span><span id="sat_intTextBox" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Phy</span><span id="sat_phy_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Coach</span><span id="sat_coach_part" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Cool</span><span id="sat_cool_down" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Total</span><span id="sat_result" class="new_set_position_2"></span></div>

                            <div class ="head_div"><span class="new_set_position">Rpe</span><span id="sat_rpe" class="new_set_position_2"></span></div>
                        </td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Tactical</td>
                            <td><span name="tactical_first" id="tactical_first"></span></td>
                            <td><span name="tactical_scnd" id="tactical_scnd"></span></td>
                            <td><span name="tactical_third" id="tactical_third"></span></td>
                            <td><span name="tactical_fourth" id="tactical_fourth"></span></td>
                            <td><span name="tactical_fifth" id="tactical_fifth"></span></td>
                            <td><span name="tactical_six" id="tactical_six"></span></td>
                            <td><span name="tactical_seven" id="tactical_seven"></span></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">technical</td>

                            <td><span name="technical_first" id="technical_first"></span></td>
                            <td><span  name="technical_scnd" id="technical_scnd"></span></td>
                            <td><span name="technical_third" id="technical_third"></span></td>
                            <td><span name="technical_fourth" id="technical_fourth"></span></td>
                            <td><span  name="technical_fifth" id="technical_fifth"></span></td>
                            <td><span name="technical_six" id="technical_six"></span></td>
                            <td><span name="technical_seven" id="technical_seven"></span></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Mental</td>
                            <td><span name="mental_first" id="mental_first"></span></td>
                            <td><span name="mental_scnd" id="mental_scnd"></span></td>
                            <td><span name="mental_third" id="mental_third"></span></td>
                            <td><span name="mental_fourth" id="mental_fourth"></span></td>
                            <td><span name="mental_fifth" id="mental_fifth"></span></td>
                            <td><span name="mental_six" id="mental_six"></span></td>
                            <td><span name="mental_seven" id="mental_seven"></span></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        </section>
         <div class="div_holder">
        <div class="calculation">
        <div class="title text-center"><h6>Training time</h6></div>
        <div class="time_dur text-center"></div>
        <div class="week_title text-center">Weekly Load</div>
        <div class="week_load text-center"></div>
        </div>
        <div id="chartContainer" style="height: 550px; max-width: 920px; margin: 0px auto 100px; display: none; float: left;"></div>
        </div>
        <div class="footer"><p>Document Preppared By Pro Football Coach.</p>
            <p>0097466053410 Or 0097477080967</p>
            <p>coachredouane@hotmail.com</p>
        </div>
    </div>
</body>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script src="js/jquery.simplecolorpicker.js"></script>
    <script type='text/javascript' src='js/pdfmake.min.js'></script>
    <script type='text/javascript' src='js/html2canvas.min.js'></script>
   <!--only numbers entered -->
   <script type="text/javascript">
    function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  });
}


// Install input filters.
//Sunday
setInputFilter(document.getElementById("sun_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//monday
setInputFilter(document.getElementById("mon_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//Tuesday
setInputFilter(document.getElementById("tues_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });
//Wednesday
setInputFilter(document.getElementById("wed_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//Thursday
setInputFilter(document.getElementById("thu_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });
//Friday


setInputFilter(document.getElementById("fri_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });


setInputFilter(document.getElementById("sat_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });



   </script>
   <script type="text/javascript">
    // var warm_up=''
    // var phy_part=''
    // var coach_part=''
    // var cool_down=''
       // $('#intTextBox').keyup(function(){
       //      var warm_up=$(this).val();
       // });
       // $('#phy-part').keyup(function(){
       //      var phy_part=$(this).val();
       // });
       // $('#coach-part').keyup(function(){
       //      var coach_part=$(this).val();
       // });
       // $('#cool-down').keyup(function(){
       //      var cool_down=$(this).val();
       //      var total = warm_up+phy_part+coach_part+cool_down;
       //      console.log(total);

       // });
       //console.log(phy_part);
       $('#sun_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#sun_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#sun_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#sun_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#sun_result').val(sun_answer);

        });
       //Monday
       $('#mon_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#mon_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#mon_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#mon_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#mon_result').val(sun_answer);

        });
       //Tuesday
        $('#tues_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#tues_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#tues_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#tues_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#tues_result').val(sun_answer);

        });

       //Wednesday
       $('#wed_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#wed_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#wed_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#wed_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#wed_result').val(sun_answer);

        });

       //Thursday
       $('#thu_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#thu_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#thu_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#thu_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#thu_result').val(sun_answer);

        });

       //Friday

       $('#fri_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#fri_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#fri_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#fri_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#fri_result').val(sun_answer);

        });
       //Sat
       $('#sat_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#sat_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#sat_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#sat_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            //console.log(myVar);
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#sat_result').val(sun_answer);

        });
   </script>

<script>

$("form#plan_data").submit(function() {
    var formData = new FormData($(this)[0]);
    $.post($(this).attr("action"), formData, function(data) {
        //alert(data);
    });
    return false;
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	var saeson_anc ='';
	var week_group ='';
	var week='';
	$(document).on('click', "a.saeson_anc", function(){
    saeson_anc=$(this).text();
    //alert(saeson_anc);
});
	$(document).on('click', "a.week_group", function(){
    week_group=$(this).text();
    //alert(week_group);
});
$(document).on('click', "a.week", function(){
    week=$(this).text();
    //alert(week);
});
$(document).on('click', "span.month", function(){
    month=$(this).text();
    //alert(month);
  // $.ajax({
  //     type:'POST',
  //    url:'general_search.php',
  //     data:{
  //       dataType: 'json',
  //       encode: true,
  //       saeson_anc:saeson_anc,
  //       week_group:week_group,
  //       week:week,
  //       month:month
  //     },
  //   })
  //     .done(function(value){
  //     	//console.log(value);
  //     var data = value.split(",");
  //     console.log(data);
  //   });
  $.ajax({
      type:'POST',
     url:'process/general_search.php',
      data:{
        dataType: 'json',
        encode: true,
        saeson_anc:saeson_anc,
        week_group:week_group,
        week:week,
        month:month
      },
    })
      .done(function(value){
      var data = value.split(",");
           // $('.acac').html(data);
      //$('#category_name').val(data[0]);
             //console.log(data[0]);
            console.log(data);
        $(".client_logo").prop("src",data[0]);
        $('#no-year-datepicker').html(data[1]);
        //$('#monday_date').val(data[1]);
        $('#monday_date').html(data[2]);
        $('#tuesday_date').html(data[3]);
        $('#wed_date').html(data[4]);
        $('#thursday_date').html(data[5]);
        $('#friday_date').html(data[6]);
        $('#sat_date').html(data[7]);
        $('#sun_color').html(data[8]);
        $('select[name="sun_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('#mon_color').html(data[9]);
        $('select[name="mon_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});

        $('#tues_color').html(data[10]);
        $('select[name="tues_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('#wed_color').html(data[11]);
        $('select[name="wed_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});

        $('#thu_color').html(data[12]);
        $('select[name="thu_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});

        $('#fri_color').html(data[13]);
        $('select[name="fri_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('#sat_color').html(data[14]);
        $('select[name="sat_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('#1st_sun_aim').html(data[15]);
        $('#1st_mon_aim').html(data[16]);
        $('#1st_tue_aim').html(data[17]);
        $('#1st_wed_aim').html(data[18]);
        $('#1st_thu_aim').html(data[19]);
        $('#1st_fri_aim').html(data[20]);
        $('#1st_sat_aim').html(data[21]);

        $('#2st_sun_aim').html(data[22]);
        $('#2st_mon_aim').html(data[23]);
        $('#2st_tue_aim').html(data[24]);
        $('#2st_wed_aim').html(data[25]);
        $('#2st_thu_aim').html(data[26]);
        $('#2st_fri_aim').html(data[27]);
        $('#2st_sat_aim').html(data[28]);


        $('#sun_comment').html(data[29]);
        $('#mon_comment').html(data[30]);
        $('#tue_comment').html(data[31]);
        $('#wed_comment').html(data[32]);
        $('#thu_comment').html(data[33]);
        $('#fri_comment').html(data[34]);
        $('#sat_comment').html(data[35]);


        $('#sun_intTextBox').html(data[36]);
       // $('#sun_intTextBox').closest("td").find("span").html(data[36]);
        $('#sun_phy_part').html(data[37]);
        $('#sun_coach_part').html(data[38]);
        $('#sun_cool_down').html(data[39]);
        $('#sun_result').html(data[40]);
        $('#sun_rpe').html(data[41]);
        var sun_time = data[40];
        var sun_total =data[40]*data[41];

        //sun_total=Math.ceil(sun_total/1000*100);

        $('#mon_intTextBox').html(data[42]);
        $('#mon_phy_part').html(data[43]);
        $('#mon_coach_part').html(data[44]);
        $('#mon_cool_down').html(data[45]);
        $('#mon_result').html(data[46]);
        $('#mon_rpe').html(data[47]);
        var mon_time = data[46];
        var mon_total =data[46]*data[47];
       // mon_total=Math.ceil(mon_total/1000*100);
        $('#tues_intTextBox').html(data[48]);
        $('#tues_phy_part').html(data[49]);
        $('#tues_coach_part').html(data[50]);
        $('#tues_cool_down').html(data[51]);
        $('#tues_result').html(data[52]);
        $('#tue_rpe').html(data[53]);
        var tue_time = data[52];
        var tue_total =data[53]*data[52];
        //tue_total=Math.ceil(tue_total/1000*100);

        $('#wed_intTextBox').html(data[54]);
        $('#wed_phy_part').html(data[55]);
        $('#wed_coach_part').html(data[56]);
        $('#wed_cool_down').html(data[57]);
        $('#wed_result').html(data[58]);
        $('#wed_rpe').html(data[59]);
        var wed_time = data[58];
        var wed_total =data[59]*data[58];

        //wed_total=Math.ceil(wed_total/1000*100);
        $('#thu_intTextBox').html(data[60]);
        $('#thu_phy_part').html(data[61]);
        $('#thu_coach_part').html(data[62]);
        $('#thu_cool_down').html(data[63]);
        $('#thu_result').html(data[64]);
        $('#thu_rpe').html(data[65]);
        var thu_time = data[64];
        var thu_total =data[65]*data[64];
       /// thu_total=Math.ceil(thu_total/1000*100);

        $('#fri_intTextBox').html(data[66]);
        $('#fri_phy_part').html(data[67]);
        $('#fri_coach_part').html(data[68]);
        $('#fri_cool_down').html(data[69]);
        $('#fri_result').html(data[70]);
        $('#fri_rpe').html(data[71]);
        var fri_time = data[70];
        var fri_total =data[71]*data[70];

        //fri_total=Math.ceil(fri_total/1000*100);

        $('#sat_intTextBox').html(data[72]);
        $('#sat_phy_part').html(data[73]);
        $('#sat_coach_part').html(data[74]);
        $('#sat_cool_down').html(data[75]);
        $('#sat_result').html(data[76]);
        $('#sat_rpe').html(data[77]);

        $('#tactical_first').html(data[78]);
        $('#tactical_scnd').html(data[79]);
        $('#tactical_third').html(data[80]);

        $('#technical_first').html(data[81]);
        $('#technical_scnd').html(data[82]);
        $('#technical_third').html(data[83]);

        $('#mental_first').html(data[84]);
        $('#mental_scnd').html(data[85]);
        $('#mental_third').html(data[86]);
        

        $('#tactical_fourth').html(data[87]);
        $('#tactical_fifth').html(data[88]);
        $('#tactical_six').html(data[89]);

        $('#technical_fourth').html(data[90]);
        $('#technical_fifth').html(data[91]);
        $('#technical_six').html(data[92]);

        $('#mental_fourth').html(data[93]);
        $('#mental_fifth').html(data[94]);
        $('#mental_six').html(data[95]);

        $('#tactical_seven').html(data[93]);
        $('#technical_seven').html(data[94]);
        $('#mental_seven').html(data[95]);
       
        var sat_time = data[76];
        var sat_total =data[77]*data[76];

       // sat_total = Math.ceil(sat_total/1000*100);
       
        grand_total = parseInt(sun_total, 10)+parseInt(mon_total, 10)+parseInt(tue_total, 10)+parseInt(wed_total, 10)+parseInt(thu_total, 10)+parseInt(fri_total, 10)+parseInt(sat_total, 10)

        // grand_total=sun_total+mon_total+tue_total+wed_total+thu_total+fri_total+sat_total;
         $('#chartContainer').show();
        fun(sun_total,mon_total,tue_total,wed_total,thu_total,fri_total,sat_total);
        $('.calculation').show();
        $('.week_load').html(grand_total);



        grand_time = parseInt(sun_time, 10)+parseInt(mon_time, 10)+parseInt(tue_time, 10)+parseInt(wed_time, 10)+parseInt(thu_time, 10)+parseInt(fri_time, 10)+parseInt(sat_time, 10)

        $('.time_dur').html(grand_time);
        // $('#thu_intTextBox').val(data[60]);
        // $('#thu_phy_part').val(data[61]);
        // $('#thu_coach_part').val(data[62]);
        // $('#thu_cool_down').val(data[63]);
        // $('#thu_result').val(data[64]);
        // $('#thu_rpe').val(data[65]);
        //$('#sat_comment').val(data[35]);


  });
	
});
});


// $("ul li.one > a").live("click",function(){
</script>
<script>
// function fun(sun_total,mon_total,tue_total,wed_total,thu_total,fri_total,sat_total) {
// //Better to construct options first and then pass it as a parameter
// var options = {
// 	animationEnabled: true,
// 	title:{
// 		text: "Performance"   
// 	},
// 	axisY:{
// 		title:"Coal (mn tonnes)"
// 	},
// 	toolTip: {
// 		shared: true,
// 		reversed: true
// 	},
// 	data: [
// 	{
// 		type: "stackedColumn",
// 		name: "SubBituminous and Lignite",
// 		showInLegend: "true",
// 		yValueFormatString: "#,##0mn tonnes",
// 		dataPoints: [
// 			{ y:  sun_total, label: "S" },
// 			{ y:  mon_total, label: "M" },
// 			{ y: tue_total, label: "T" },
// 			{ y: wed_total, label: "W" },
// 			{ y: thu_total, label: "T" },
// 			{ y: fri_total, label: "F" },
// 			{ y: sat_total, label: "S" }
// 		]
// 	}]
// };

// $("#chartContainer").CanvasJSChart(options);
// }
// function fun(sun_total,mon_total,tue_total,wed_total,thu_total,fri_total,sat_total)
// {
// var options = {
//     animationEnabled: true,
//     title:{
//         text: "Coal Reserves of Countries"   
//     },
//     axisY:{
//         title:"Coal (mn tonnes)"
//     },
//     toolTip: {
//         shared: true,
//         reversed: true
//     },
//     data: [
//     {
//         type: "stackedColumn",
//         name: "SubBituminous and Lignite",
//         showInLegend: "true",
//         yValueFormatString: "#,##0mn tonnes",
//         dataPoints: [
//             { y: 135305 , label: "USA" },
//             { y: 107922, label: "Russia" },
//             { y: 52300, label: "China" },
//             { y: 3360, label: "India" },
//             { y: 39900, label: "Australia" },
//             { y: 0, label: "SA" }
//         ]
//     }]
// };

// $("#chartContainer").CanvasJSChart(options);
// }
function fun(sun_total,mon_total,tue_total,wed_total,thu_total,fri_total,sat_total) {
//console.log(sun_total);
//Better to construct options first and then pass it as a parameter
var options = {
    animationEnabled: true,
    title: {
        text: "Performance",                
        fontColor: "Peru"
    },  
    axisY: {
        tickThickness: 0,
        lineThickness: 0,
        valueFormatString: " ",
        gridThickness: 0                    
    },
    axisX: {
        tickThickness: 0,
        lineThickness: 0,
        labelFontSize: 18,
        labelFontColor: "Peru"              
    },
    data: [{
        indexLabelFontSize: 26,
        toolTipContent: "<span style=\"color:#62C9C3\">{indexLabel}:</span> <span style=\"color:#CD853F\"><strong>{y}</strong></span>",
        indexLabelPlacement: "inside",
        indexLabelFontColor: "white",
        indexLabelFontWeight: 600,
        indexLabelFontFamily: "Verdana",
        color: "#62C9C3",
        type: "bar",
        dataPoints: [
            { y: sun_total, label: sun_total, indexLabel: "S" },
            { y: mon_total, label: mon_total, indexLabel: "M" },
            { y: tue_total, label: tue_total, indexLabel: "T" },
            { y: wed_total, label: wed_total, indexLabel: "W" },
            { y: thu_total, label: thu_total, indexLabel: "T" },
            { y: fri_total, label: fri_total, indexLabel: "F" },
            { y: sat_total, label: sat_total, indexLabel: "S" }
            
        ]
    }]
};

$("#chartContainer").CanvasJSChart(options);
}
</script>
<script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>


    
    <script type="text/javascript">
            $("body").on("click", "#download", function () {
                $('.nav-link').css('color', 'red !important');
                // var img=canvas.toDataURL("image/png");
                html2canvas($('.page-wrap')[0], {
                    onrendered: function (canvas) {
                        var data = canvas.toDataURL();
                        var docDefinition = {
                            content: [{
                                image: data,
                                width: 500
                            }]
                        };
                        pdfMake.createPdf(docDefinition).download("Table.pdf");
                    }
                });
                 //$('.page-wrap').css({color: white});
                
            });
    </script>
</html>


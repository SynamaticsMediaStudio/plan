<?php
	//echo "hi";
	//exit;
	include_once("DBAccess.php");
	class DBManager extends DBAccess
	{
		function __construct(){
			
		}
		function escape($v){
			return mysqli_real_escape_string($this->con,$v);
		}


		function validate_data($user_name,$user_pass)
		{
			$this->connectToDB();
			$sql="SELECT * FROM login WHERE username='".$user_name."' AND password='".$user_pass."'";
			//echo $sql;exit;
			$result=$this->CustomQuery($sql);
			//print_r($result);
			//exit;
			$this->DBDisconnect();
			return $result;
		}
		
		
		function insert_weekly_plan($season,$age_group,$week,$month,$target_file,$sun_date,$monday_date,$tuesday_date,$wed_date,$thursday_date,$friday_date,$sat_date,$sun_color,$mon_color,$tues_color,$wed_color,$thu_color,$fri_color,$sat_color,$sunday_aim,$monday_aim,$tues_aim,$wed_aim,$thurs_aim,$fri_aim,$sat_aim,$scnd_sunday_aim,$scnd_monday_aim,$scnd_tues_aim,$scnd_wed_aim,$second_thu_aim,$scnd_fri_aim,$second_sat_aim,$sunday_detail,$mon_detail,$tues_detail,$wed_detail,$thurs_detail,$fri_detail,$sat_detail,$fst_sun_warm_up,$fst_sun_phy_part,$fst_sun_coach_part,$fst_sun_cool_part,$fst_sun_total,$fst_sun_rpe_part,$scnd_sun_warm_up,$scnd_sun_phy_part,$scnd_sun_coach_part,$scnd_sun_cool_part,$scnd_sun_total,$scnd_sun_rpe_part,$third_sun_warm_up,$third_sun_phy_part,$third_sun_coach_part,$third_sun_cool_part,$third_sun_total,$third_sun_rpe_part,$fourth_sun_warm_up,$fourth_sun_phy_part,$fourth_sun_coach_part,$fourth_sun_cool_part,$fourth_sun_total,$fourth_sun_rpe_part,$five_sun_warm_up,$five_sun_phy_part,$five_sun_coach_part,$five_sun_cool_part,$five_sun_total,$five_sun_rpe_part,$six_sun_warm_up,$six_sun_phy_part,$six_sun_coach_part,$six_sun_cool_part,$six_sun_total,$six_sun_rpe_part,$seven_sun_warm_up,$seven_sun_phy_part,$seven_sun_coach_part,$seven_sun_cool_part,$seven_sun_total,$seven_sun_rpe_part,$tactical_first,$tactical_scnd,$tactical_third,$technical_first,$technical_scnd,$technical_third,$mental_first,$mental_scnd,$mental_third,$tactical_fourth,$tactical_fifth,$tactical_six,$technical_fourth,$technical_fifth,$technical_six,$mental_fourth,$mental_fifth,$mental_six,$tactical_seven,$technical_seven,$mental_seven)
		{
			$this->connectToDB();
			$fld_status=1;
			$query = $this->DBlink->prepare("INSERT INTO weekly_plan(fld_season,fld_age_group,fld_week,fld_month,fld_image,fld_sun_date,fld_mon_date,fld_tues_date,fld_wed_date,fld_thu_date,fld_fri_date,fld_sat_date,fld_sun_intensity,fld_mon_intensity,fld_tues_intensity,fld_wed_intensity,fld_thu_intensity,fld_fri_intensity,fld_sat_intensity,fld_sunday_aim,fld_monday_aim,fld_tues_aim,fld_wed_aim,fld_thurs_aim,fld_fri_aim,fld_sat_aim,fld_scnd_sunday_aim,fld_scnd_monday_aim,fld_scnd_tues_aim,fld_scnd_wed_aim,fld_scnd_thurs_aim,fld_scnd_fri_aim,fld_scnd_sat_aim,fld_sunday_detail,fld_mon_detail,fld_tues_detail,fld_wed_detail,fld_thurs_detail,fld_fri_detail,fld_sat_detail,fld_fst_sun_warm_up,fld_fst_sun_phy_part,fld_fst_sun_coach_part,fld_fst_sun_cool_part,fld_fst_sun_total,fld_fst_sun_rpe_part,fld_scnd_sun_warm_up,fld_scnd_sun_phy_part,fld_scnd_sun_coach_part,fld_scnd_sun_cool_part,fld_scnd_sun_total,fld_scnd_sun_rpe_part,fld_third_sun_warm_up,fld_third_sun_phy_part,fld_third_sun_coach_part,fld_third_sun_cool_part,fld_third_sun_total,fld_third_sun_rpe_part,fld_fourth_sun_warm_up,fld_fourth_sun_phy_part,fld_fourth_sun_coach_part,fld_fourth_sun_cool_part,fld_fourth_sun_total,fld_fourth_sun_rpe_part,fld_five_sun_warm_up,fld_five_sun_phy_part,fld_five_sun_coach_part,fld_five_sun_cool_part,fld_five_sun_total,fld_five_sun_rpe_part,fld_six_sun_warm_up,fld_six_sun_phy_part,fld_six_sun_coach_part,fld_six_sun_cool_part,fld_six_sun_total,fld_six_sun_rpe_part,fld_seven_sun_warm_up,fld_seven_sun_phy_part,fld_seven_sun_coach_part,fld_seven_sun_cool_part,fld_seven_sun_total,fld_seven_sun_rpe_part,fld_tactical_first,fld_tactical_scnd,fld_tactical_third,fld_technical_first,fld_technical_scnd,fld_technical_third,fld_mental_first,fld_mental_scnd,fld_mental_third,fld_tactical_fourth,fld_tactical_fifth,fld_tactical_six,fld_technical_fourth,fld_technical_fifth,fld_technical_six,fld_mental_fourth,fld_mental_fifth,fld_mental_six,fld_tactical_seven,fld_technical_seven,fld_mental_seven) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			$query->bind_param("sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss",$season,$age_group,$week,$month,$target_file,$sun_date,$monday_date,$tuesday_date,$wed_date,$thursday_date,$friday_date,$sat_date,$sun_color,$mon_color,$tues_color,$wed_color,$thu_color,$fri_color,$sat_color,$sunday_aim,$monday_aim,$tues_aim,$wed_aim,$thurs_aim,$fri_aim,$sat_aim,$scnd_sunday_aim,$scnd_monday_aim,$scnd_tues_aim,$scnd_wed_aim,$second_thu_aim,$scnd_fri_aim,$second_sat_aim,$sunday_detail,$mon_detail,$tues_detail,$wed_detail,$thurs_detail,$fri_detail,$sat_detail,$fst_sun_warm_up,$fst_sun_phy_part,$fst_sun_coach_part,$fst_sun_cool_part,$fst_sun_total,$fst_sun_rpe_part,$scnd_sun_warm_up,$scnd_sun_phy_part,$scnd_sun_coach_part,$scnd_sun_cool_part,$scnd_sun_total,$scnd_sun_rpe_part,$third_sun_warm_up,$third_sun_phy_part,$third_sun_coach_part,$third_sun_cool_part,$third_sun_total,$third_sun_rpe_part,$fourth_sun_warm_up,$fourth_sun_phy_part,$fourth_sun_coach_part,$fourth_sun_cool_part,$fourth_sun_total,$fourth_sun_rpe_part,$five_sun_warm_up,$five_sun_phy_part,$five_sun_coach_part,$five_sun_cool_part,$five_sun_total,$five_sun_rpe_part,$six_sun_warm_up,$six_sun_phy_part,$six_sun_coach_part,$six_sun_cool_part,$six_sun_total,$six_sun_rpe_part,$seven_sun_warm_up,$seven_sun_phy_part,$seven_sun_coach_part,$seven_sun_cool_part,$seven_sun_total,$seven_sun_rpe_part,$tactical_first,$tactical_scnd,$tactical_third,$technical_first,$technical_scnd,$technical_third,$mental_first,$mental_scnd,$mental_third,$tactical_fourth,$tactical_fifth,$tactical_six,$technical_fourth,$technical_fifth,$technical_six,$mental_fourth,$mental_fifth,$mental_six,$tactical_seven,$technical_seven,$mental_seven);
			$query->execute();
			$query->close();
			$this->DBDisconnect();
			$this->connectToDB();
			return $query;
		}

		
		function sample($query)
		{
			//echo 'hi';
			$this->connectToDB();
            //$sql="SELECT * FROM  tbl_job_category ";			//echo $sql;exit;
			$result=$this->CustomQuery($query);
			//print_r($result);
			//exit;
			$this->DBDisconnect();
			return $result;	
		}
		
		function update_weekly_plan($season,$age_group,$week,$month,$sun_date,$monday_date,$tuesday_date,$wed_date,$thursday_date,$friday_date,$sat_date,$sun_color,$mon_color,$tues_color,$wed_color,$thu_color,$fri_color,$sat_color,$sunday_aim,$monday_aim,$tues_aim,$wed_aim,$thurs_aim,$fri_aim,$sat_aim,$scnd_sunday_aim,$scnd_monday_aim,$scnd_tues_aim,$scnd_wed_aim,$second_thu_aim,$scnd_fri_aim,$second_sat_aim,$sunday_detail,$mon_detail,$tues_detail,$wed_detail,$thurs_detail,$fri_detail,$sat_detail,$fst_sun_warm_up,$fst_sun_phy_part,$fst_sun_coach_part,$fst_sun_cool_part,$fst_sun_total,$fst_sun_rpe_part,$scnd_sun_warm_up,$scnd_sun_phy_part,$scnd_sun_coach_part,$scnd_sun_cool_part,$scnd_sun_total,$scnd_sun_rpe_part,$third_sun_warm_up,$third_sun_phy_part,$third_sun_coach_part,$third_sun_cool_part,$third_sun_total,$third_sun_rpe_part,$fourth_sun_warm_up,$fourth_sun_phy_part,$fourth_sun_coach_part,$fourth_sun_cool_part,$fourth_sun_total,$fourth_sun_rpe_part,$five_sun_warm_up,$five_sun_phy_part,$five_sun_coach_part,$five_sun_cool_part,$five_sun_total,$five_sun_rpe_part,$six_sun_warm_up,$six_sun_phy_part,$six_sun_coach_part,$six_sun_cool_part,$six_sun_total,$six_sun_rpe_part,$seven_sun_warm_up,$seven_sun_phy_part,$seven_sun_coach_part,$seven_sun_cool_part,$seven_sun_total,$seven_sun_rpe_part,$tactical_first,$tactical_scnd,$tactical_third,$technical_first,$technical_scnd,$technical_third,$mental_first,$mental_scnd,$mental_third,$tactical_fourth,$tactical_fifth,$tactical_six,$technical_fourth,$technical_fifth,$technical_six,$mental_fourth,$mental_fifth,$mental_six,$tactical_seven,$technical_seven,$mental_seven)
		{
			$this->connectToDB();
			$sql="UPDATE `weekly_plan` SET`fld_sun_date`=?,`fld_mon_date`=?,`fld_tues_date`=?,`fld_wed_date`=?,`fld_thu_date`=?,`fld_fri_date`=?,`fld_sat_date`=?,`fld_sun_intensity`=?,`fld_mon_intensity`=?,`fld_tues_intensity`=?,`fld_wed_intensity`=?,`fld_thu_intensity`=?,`fld_fri_intensity`=?,`fld_sat_intensity`=?,`fld_sunday_aim`=?,`fld_monday_aim`=?,`fld_tues_aim`=?,`fld_wed_aim`=?,`fld_thurs_aim`=?,`fld_fri_aim`=?,`fld_sat_aim`=?,`fld_scnd_sunday_aim`=?,`fld_scnd_monday_aim`=?,`fld_scnd_tues_aim`=?,`fld_scnd_wed_aim`=?,`fld_scnd_thurs_aim`=?,`fld_scnd_fri_aim`=?,`fld_scnd_sat_aim`=?,`fld_sunday_detail`=?,`fld_mon_detail`=?,`fld_tues_detail`=?,`fld_wed_detail`=?,`fld_thurs_detail`=?,`fld_fri_detail`=?,`fld_sat_detail`=?,`fld_fst_sun_warm_up`=?,`fld_fst_sun_phy_part`=?,`fld_fst_sun_coach_part`=?,`fld_fst_sun_cool_part`=?,`fld_fst_sun_total`=?,`fld_fst_sun_rpe_part`=?,`fld_scnd_sun_warm_up`=?,`fld_scnd_sun_phy_part`=?,`fld_scnd_sun_coach_part`=?,`fld_scnd_sun_cool_part`=?,`fld_scnd_sun_total`=?,`fld_scnd_sun_rpe_part`=?,`fld_third_sun_warm_up`=?,`fld_third_sun_phy_part`=?,`fld_third_sun_coach_part`=?,`fld_third_sun_cool_part`=?,`fld_third_sun_total`=?,`fld_third_sun_rpe_part`=?,`fld_fourth_sun_warm_up`=?,`fld_fourth_sun_phy_part`=?,`fld_fourth_sun_coach_part`=?,`fld_fourth_sun_cool_part`=?,`fld_fourth_sun_total`=?,`fld_fourth_sun_rpe_part`=?,`fld_five_sun_warm_up`=?,`fld_five_sun_phy_part`=?,`fld_five_sun_coach_part`=?,`fld_five_sun_cool_part`=?,`fld_five_sun_total`=?,`fld_five_sun_rpe_part`=?,`fld_six_sun_warm_up`=?,`fld_six_sun_phy_part`=?,`fld_six_sun_coach_part`=?,`fld_six_sun_cool_part`=?,`fld_six_sun_total`=?,`fld_six_sun_rpe_part`=?,`fld_seven_sun_warm_up`=?,`fld_seven_sun_phy_part`=?,`fld_seven_sun_coach_part`=?,`fld_seven_sun_cool_part`=?,`fld_seven_sun_total`=?,`fld_seven_sun_rpe_part`=?,`fld_tactical_first`=?,`fld_tactical_scnd`=?,`fld_tactical_third`=?,`fld_technical_first`=?,`fld_technical_scnd`=?,`fld_technical_third`=?,`fld_mental_first`=?,`fld_mental_scnd`=?,`fld_mental_third`=?,`fld_tactical_fourth`=?,`fld_tactical_fifth`=?,`fld_tactical_six`=?,`fld_technical_fourth`=?,`fld_technical_fifth`=?,`fld_technical_six`=?,`fld_mental_fourth`=?,`fld_mental_fifth`=?,`fld_mental_six`=?,`fld_tactical_seven`=?,`fld_technical_seven`=?,`fld_mental_seven`=? where `fld_season`=? AND `fld_age_group`=? AND fld_week=? AND  	fld_month=?";
			$query = $this->DBlink->prepare("$sql");
			$query->bind_param("ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss",$sun_date,$monday_date,$tuesday_date,$wed_date,$thursday_date,$friday_date,$sat_date,$sun_color,$mon_color,$tues_color,$wed_color,$thu_color,$fri_color,$sat_color,$sunday_aim,$monday_aim,$tues_aim,$wed_aim,$thurs_aim,$fri_aim,$sat_aim,$scnd_sunday_aim,$scnd_monday_aim,$scnd_tues_aim,$scnd_wed_aim,$second_thu_aim,$scnd_fri_aim,$second_sat_aim,$sunday_detail,$mon_detail,$tues_detail,$wed_detail,$thurs_detail,$fri_detail,$sat_detail,$fst_sun_warm_up,$fst_sun_phy_part,$fst_sun_coach_part,$fst_sun_cool_part,$fst_sun_total,$fst_sun_rpe_part,$scnd_sun_warm_up,$scnd_sun_phy_part,$scnd_sun_coach_part,$scnd_sun_cool_part,$scnd_sun_total,$scnd_sun_rpe_part,$third_sun_warm_up,$third_sun_phy_part,$third_sun_coach_part,$third_sun_cool_part,$third_sun_total,$third_sun_rpe_part,$fourth_sun_warm_up,$fourth_sun_phy_part,$fourth_sun_coach_part,$fourth_sun_cool_part,$fourth_sun_total,$fourth_sun_rpe_part,$five_sun_warm_up,$five_sun_phy_part,$five_sun_coach_part,$five_sun_cool_part,$five_sun_total,$five_sun_rpe_part,$six_sun_warm_up,$six_sun_phy_part,$six_sun_coach_part,$six_sun_cool_part,$six_sun_total,$six_sun_rpe_part,$seven_sun_warm_up,$seven_sun_phy_part,$seven_sun_coach_part,$seven_sun_cool_part,$seven_sun_total,$seven_sun_rpe_part,$tactical_first,$tactical_scnd,$tactical_third,$technical_first,$technical_scnd,$technical_third,$mental_first,$mental_scnd,$mental_third,$tactical_fourth,$tactical_fifth,$tactical_six,$technical_fourth,$technical_fifth,$technical_six,$mental_fourth,$mental_fifth,$mental_six,$tactical_seven,$technical_seven,$mental_seven,$season,$age_group,$week,$month);
			$query->execute();
			$query->close();
			$this->DBDisconnect();
			return;
		}
		
		function deleterecById($season,$age_group,$week,$month)
		{
			$this->connectToDB();
			$sql="delete from weekly_plan where fld_season='$season' AND  	fld_age_group='$age_group' AND fld_week='$week' AND fld_month='$month'";
			$result=$this->CustomModify($sql);
			$this->DBDisconnect();
			return $result;
		}

		function insert_player_detail($player_num,$name,$f_name,$Nationality,$Evaluation,$Academy,$position,$national_team,$contact_number,$father_contact,$foot_info,$target_file,$player_cat,$seas)
		{
			$this->connectToDB();
			$fld_status=1;
			$query = $this->DBlink->prepare("INSERT INTO player_info(player_number,fld_player_name,fld_player_father_name,fld_player_Nationality,fld_player_position,fld_player_evaluation,fld_player_academy,fld_national_team,fld_player_contact_number,fld_player_father_contact,fld_player_foot_info,fld_player_image,fld_cat, 	fld_season) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			$query->bind_param("ssssssssssssss",$player_num,$name,$f_name,$Nationality,$position,$Evaluation,$Academy,$national_team,$contact_number,$father_contact,$foot_info,$target_file,$player_cat,$seas);
			$query->execute();
			$query->close();
			$this->DBDisconnect();
			$this->connectToDB();
			return $query;
		}

		function insert_player_eval($player_num,$name,$f_name,$Nationality,$Evaluation,$Academy,$position,$national_team,$contact_number,$father_contact,$foot_info,$target_file,$player_cat,$seas,$tac_eng_title,$tac_arabic,$tac_eval,$tech_eng_title,$tech_arabic,$tech_eval,$phy_eng_title,$phy_arabic,$phy_eval,$psy_eng_title,$psy_arabic,$psy_eval,$remarks,$player_sel)
		{

			$this->connectToDB();
			$fld_status=1;
			$query = $this->DBlink->prepare("INSERT INTO player_evaluation(player_number,fld_player_name,fld_player_father_name,fld_player_Nationality,fld_player_position,fld_player_evaluation,fld_player_academy,fld_national_team,fld_player_contact_number,fld_player_father_contact,fld_player_foot_info,fld_player_image,fld_cat,fld_season,fld_tactical,fld_tac_arabic,fld_tac_eval,fld_technical,fld_tech_arabic,fld_tech_eval,fld_physical,fld_phy_arabic,fld_phy_eval,fld_psychosocial,fld_psy_arabic,fld_psy_eval,fld_remarks,fld_player_sel) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			$query->bind_param("ssssssssssssssssssssssssssss",$player_num,$name,$f_name,$Nationality,$Evaluation,$Academy,$position,$national_team,$contact_number,$father_contact,$foot_info,$target_file,$player_cat,$seas,$tac_eng_title,$tac_arabic,$tac_eval,$tech_eng_title,$tech_arabic,$tech_eval,$phy_eng_title,$phy_arabic,$phy_eval,$psy_eng_title,$psy_arabic,$psy_eval,$remarks,$player_sel);
			$query->execute();
			$query->close();
			$this->DBDisconnect();
			$this->connectToDB();
			return $query;
		}
		function update_player_eval($player_num,$name,$f_name,$Nationality,$Evaluation,$Academy,$position,$national_team,$contact_number,$father_contact,$foot_info,$target_file,$player_cat,$seas,$tac_eng_title,$tac_arabic,$tac_eval,$tech_eng_title,$tech_arabic,$tech_eval,$phy_eng_title,$phy_arabic,$phy_eval,$psy_eng_title,$psy_arabic,$psy_eval,$remarks,$player_sel)
		{
			$this->connectToDB();
			$sql="UPDATE `player_evaluation` SET`fld_player_name`=?,`fld_player_father_name`=?,`fld_player_Nationality`=?,`fld_player_position`=?,`fld_player_evaluation`=?,`fld_player_academy`=?,`fld_national_team`=?,`fld_player_contact_number`=?,`fld_player_father_contact`=?,`fld_player_foot_info`=?,`fld_player_image`=?,`fld_cat`=?,`fld_season`=?,`fld_tactical`=?,`fld_tac_arabic`=?,`fld_tac_eval`=?,`fld_technical`=?,`fld_tech_arabic`=?,`fld_tech_eval`=?,`fld_physical`=?,`fld_phy_arabic`=?,`fld_phy_eval`=?,`fld_psychosocial`=?,`fld_psy_arabic`=?,`fld_psy_eval`=?,`fld_remarks`=?,`fld_player_sel`=? Where player_number=?";
			$query = $this->DBlink->prepare("$sql");
			$query->bind_param("ssssssssssssssssssssssssssss",$name,$f_name,$Nationality,$Evaluation,$Academy,$position,$national_team,$contact_number,$father_contact,$foot_info,$target_file,$player_cat,$seas,$tac_eng_title,$tac_arabic,$tac_eval,$tech_eng_title,$tech_arabic,$tech_eval,$phy_eng_title,$phy_arabic,$phy_eval,$psy_eng_title,$psy_arabic,$psy_eval,$remarks,$player_sel,$player_num);
			$query->execute();
			$query->close();
			$this->DBDisconnect();
			$this->connectToDB();
			return $query;
			
		}
		function get_player_info()
		{
			//echo "hi";
			$this->connectToDB();
			$sql='SELECT * FROM `player_info`';
			//echo $sql;
			//print_r($sql);
			//exit;
			$result=$this->CustomQuery($sql);
			//print_r( $result); exit;
			$this->DBDisconnect();
			return $result;
		}
		function update_player_detail($update_id,$player_num,$name,$f_name,$Nationality,$Evaluation,$Academy,$position,$national_team,$contact_number,$father_contact,$foot_info,$player_cat,$seas,$player_image)
		{
			$this->connectToDB();
			$sql="UPDATE `player_info` SET `player_number`=?,`fld_player_name`=?,`fld_player_father_name`=?,`fld_player_Nationality`=?,`fld_player_position`=?,`fld_player_evaluation`=?,`fld_player_academy`=?,`fld_national_team`=?,`fld_player_contact_number`=?,`fld_player_father_contact`=?,`fld_player_foot_info`=?,`fld_player_image`=?,`fld_cat`=?,`fld_season`=? WHERE `fld_id`=?";
			$query = $this->DBlink->prepare("$sql");
			$query->bind_param("ssssssssssssssi",$player_num,$name,$f_name,$Nationality,$position,$Evaluation,$Academy,$national_team,$contact_number,$father_contact,$foot_info,$player_image,$player_cat,$seas,$update_id);
			$query->execute();
			$query->close();
			$this->DBDisconnect();
			return;
		}
		function deletedataById($delete_id)
		{
			$this->connectToDB();
			$sql="delete from player_info where fld_id=$delete_id";
			$result=$this->CustomModify($sql);
			$this->DBDisconnect();
			return $result;
		}
}
	

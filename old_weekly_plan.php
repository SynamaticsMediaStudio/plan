<?php
include_once("secure.php");
  error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Weekly Plan</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="css/style.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>
<body>
<div class="wrapper">
    <section class="nav_section" style="min-height: 430vh;">
        <div class="container-fluid">
            <form class="form-control" name="form" method="post" enctype="multipart/form-data" id="plan_data" action="insert_weekly_plan.php">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="images/Pro_Football_Coach_Logo.png"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          season
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="season" style="border: none;" required id="season">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Age Group
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="age_group" style="border: none;" id="age_group">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Week
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="week" style="border: none;" required id="week">
        </div>
      </li>
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Month
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <input type="text" id="new_date" class="new_date form-control" style="border: none;" required  name="month">
        </div>
      </li> -->
      <li class="nav-item newdropdown">
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
        <li class="nav-item newdropdown">
        <select class="form-control" name="month">
            <option selected="selected">Month</option>
            <option value="january">January</option>
            <option value="February">February</option>
            <option value="March">March</option>
            <option value="April">April</option>
            <option value="May">May</option>
            <option value="June">June</option>
            <option value="July">July</option>
            <option value="August">August</option>
            <option value="September">September</option>
            <option value="October">October</option>
            <option value="November">November</option>
            <option value="December">December</option>

        </select>
            </li>
      <li class="nav-item dropdown">
       
        <input type="file" class="file" name="file" id="file" required>
      </li>
      <li>

      </li>
    </ul>
  </div>
</nav>
        
            <h1 align="top" style="margin: 100px 0 0 0;">Weekly Plan</h1>
            <button type="submit" class="btn btn-success save" id="save">Save</button>
                <a href="fetch_plan.php" class="btn btn-info save" id="save" style="float: right;margin: 0 9px 24px 18px;">View</a>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Day</th>
                            <th scope="col">Sunday</th>
                            <th scope="col">Monday</th>
                            <th scope="col">Tuesday</th>
                            <th scope="col">Wednesday</th>
                            <th scope="col">Thursday</th>
                            <th scope="col">Friday</th>
                            <th scope="col">Saturday</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="first_tb_data">Date</td>
                            <td class="color_align">
                                <select class="form-control" name="sun_date">
                                    <?php
                                    for ($i=1; $i <=31 ; $i++) { 
                                        # code...
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                    
                                </select>
                            </td>
                            <td class="color_align">
                                <select class="form-control" name="monday_date">
                                    <?php
                                    for ($i=1; $i <=31 ; $i++) { 
                                        # code...
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                    
                                </select></td>
                            <td class="color_align">
                                <select class="form-control" name="tuesday_date">
                                    <?php
                                    for ($i=1; $i <=31 ; $i++) { 
                                        # code...
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                    
                                </select>
                            </td>
                            <td class="color_align">
                                <select class="form-control" name="wed_date">
                                    <?php
                                    for ($i=1; $i <=31 ; $i++) { 
                                        # code...
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                    
                                </select></td>
                            <td class="color_align">
                                <select class="form-control" name="thursday_date">
                                    <?php
                                    for ($i=1; $i <=31 ; $i++) { 
                                        # code...
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                    
                                </select>
                            </td>
                            <td class="color_align">
                                <select class="form-control" name="friday_date">
                                    <?php
                                    for ($i=1; $i <=31 ; $i++) { 
                                        # code...
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                    
                                </select>
                            </td>
                            <td class="color_align">
                                <select class="form-control" name="sat_date">
                                    <?php
                                    for ($i=1; $i <=31 ; $i++) { 
                                        # code...
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                    ?>
                                    
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Intensity</td>
                            <td class="color_align">
                            <select name="sun_color" required id="sun_color">
                                <option value="#fff">White</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="mon_color" required id="mon_color" class="color_align">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="tues_color" required id="tues_color">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="wed_color" required id="wed_color">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="thu_color" required id="thu_color">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="fri_color" required id="fri_color">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                            <td class="color_align">
                            <select name="sat_color" required id="sat_color">
                                <option value="#fff">Green</option>
                                <option value="#FFA500">Orange</option>
                                <option value="#FFFF00">Yellow</option>
                                <option value="#FF0000">Red</option>
                                <option value="#000">Black</option>
                                <option value="#00ff00">Green</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Aim</td>
                            <td><Input type="text" name = "sunday_aim" class="form-control" id="1st_sun_aim"></td>
                            <td><Input type="text" name = "monday_aim" class="form-control" id="1st_mon_aim"></td>
                            <td><Input type="text" name = "tues_aim" class="form-control" id="1st_tue_aim"></td>
                            <td><Input type="text" name = "wed_aim" class="form-control" id="1st_wed_aim"></td>
                            <td><Input type="text" name = "thurs_aim" class="form-control" id="1st_thu_aim"></td>
                            <td><Input type="text" name = "fri_aim" class="form-control"  id="1st_fri_aim"></td>
                            <td><Input type="text" name = "sat_aim" class="form-control" id="1st_sat_aim"></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Aim</td>
                            <td><Input type="text" name = "scnd_sunday_aim" class="form-control" id="2st_sun_aim"></td>
                            <td><Input type="text" name = "scnd_monday_aim" class="form-control" id="2st_mon_aim"></td>
                            <td><Input type="text" name = "scnd_tues_aim" class="form-control" id="2st_tue_aim"></td>
                            <td><Input type="text" name = "scnd_wed_aim" class="form-control" id="2st_wed_aim"></td>
                            <td><Input type="text" name = "scnd_thurs_aim" class="form-control" id="2st_thu_aim"></td>
                            <td><Input type="text" name = "scnd_fri_aim" class="form-control" id="2st_fri_aim"></td>
                            <td><Input type="text" name = "scnd_sat_aim" class="form-control" id="2st_sat_aim"> </td>
                            
                        </tr>
                        <tr>
                            <td class="first_tb_data">Training Details</td>
                            <td><textarea class="form-control" rows="2" id="sun_comment" name="sunday_detail"></textarea></td>
                            <td><textarea class="form-control" rows="2" id="mon_comment" name="mon_detail"></textarea></td>
                            <td><textarea class="form-control" rows="2" id="tue_comment" name="tues_detail"></textarea></td>
                            <td><textarea class="form-control" rows="2" id="wed_comment" name="wed_detail"></textarea></td>
                            <td><textarea class="form-control" rows="2" id="thu_comment" name="thurs_detail"></textarea></td>
                            <td><textarea class="form-control" rows="2" id="fri_comment" name="fri_detail"></textarea></td>
                            <td><textarea class="form-control" rows="2" id="sat_comment" name="sat_detail"></textarea></td>

                            
                        </tr>
                        <tr>
                            <td class="first_tb_data">Overall Session Detail</td>
                            <td><span style="float: left; font-size: 14px;">Topic With Dur</span>
                                <br>
                            <label class="set_label">Warm up:</label>
                            <input type="text" name="1st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="sun_intTextBox">
                            <label class="set_label">Physical Part:</label>
                            <input type="text" name="1st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="sun_phy_part">
                            <label class="set_label">Coach Part:</label>
                            <input type="text" name="1st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="sun_coach_part">
                            <label class="set_label">Cool Down:</label>
                            <input type="text" name="1st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="sun_cool_down">
                            <label class="set_label">Total:</label>
                            <input type="text" name="1st_sun_total" placeholder="Total" class="form-control aa" id="sun_result">
                            <label class="set_label">RPE:</label>
                            <input type="text" name="1st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="sun_rpe">
                        </td>
                        <td><span style="float: left; font-size: 14px;">Topic With Dur</span>
                            <br>
                            <label class="set_label">Warm Up:</label>
                            <input type="text" name="2st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="mon_intTextBox">
                            <label class="set_label">Physical Part:</label>
                            <input type="text" name="2st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="mon_phy_part">
                            <label class="set_label">Coach Part:</label>
                            <input type="text" name="2st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="mon_coach_part">
                            <label class="set_label">Cool Down:</label>
                            <input type="text" name="2st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="mon_cool_down">
                            <label class="set_label">Total:</label>
                            <input type="text" name="2st_sun_total" placeholder="Total" class="form-control aa" id="mon_result">
                            <label class="set_label">RPE:</label>
                            <input type="text" name="2st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="mon_rpe">
                        </td>
                          
                            <td><span style="float: left; font-size: 14px;">Topic With Dur</span>
                                <br>
                            <label class="set_label">Warm Up:</label>
                            <input type="text" name="3st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="tues_intTextBox">
                            <label class="set_label">Physical Part:</label>
                            <input type="text" name="3st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="tues_phy_part">
                            <label class="set_label">Coach Part:</label>
                            <input type="text" name="3st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="tues_coach_part">
                            <label class="set_label">Cool Down:</label>
                            <input type="text" name="3st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="tues_cool_down">
                            <label class="set_label">Total:</label>
                            <input type="text" name="3st_sun_total" placeholder="Total" class="form-control aa" id="tues_result">
                            <label class="set_label">RPE:</label>
                            <input type="text" name="3st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="tue_rpe">
                        </td>
                         <td><span style="float: left; font-size: 14px;">Topic With Dur</span>
                            <br>
                            <label class="set_label">Warm Up:</label>
                            <input type="text" name="4st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="wed_intTextBox">
                            <label class="set_label">Physical Part:</label>
                            <input type="text" name="4st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="wed_phy_part">
                            <label class="set_label">Coach Part:</label>
                            <input type="text" name="4st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="wed_coach_part">
                            <label class="set_label">Cool Down:</label>
                            <input type="text" name="4st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="wed_cool_down">
                            <label class="set_label">Total:</label>
                            <input type="text" name="4st_sun_total" placeholder="Total" class="form-control aa" id="wed_result">
                            <label class="set_label">RPE:</label>
                            <input type="text" name="4st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="wed_rpe">
                        </td>

                        <td><span style="float: left; font-size: 14px;">Topic With Dur</span>
                            <br>
                            <label class="set_label">Warm Up:</label>
                            <input type="text" name="5st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="thu_intTextBox">
                            <label class="set_label">Physical Part:</label>
                            <input type="text" name="5st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="thu_phy_part">
                            <label class="set_label">Coach Part:</label>
                            <input type="text" name="5st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="thu_coach_part">
                            <label class="set_label">Cool Down:</label>
                            <input type="text" name="5st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="thu_cool_down">
                            <label class="set_label">Total:</label>
                            <input type="text" name="5st_sun_total" placeholder="Total" class="form-control aa" id="thu_result">
                            <label class="set_label">RPE:</label>
                            <input type="text" name="5st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="thu_rpe">
                        </td>


                        <td><span style="float: left; font-size: 14px;">Topic With Dur</span>
                            <br>
                            <label class="set_label">Warm Up:</label>
                            <input type="text" name="6st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="fri_intTextBox">
                            <label class="set_label">Physical Part:</label>
                            <input type="text" name="6st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="fri_phy_part">
                            <label class="set_label">Coach Part:</label>
                            <input type="text" name="6st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="fri_coach_part">
                            <label class="set_label">Cool Down:</label>
                            <input type="text" name="6st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="fri_cool_down">
                            <label class="set_label">Total:</label>
                            <input type="text" name="6st_sun_total" placeholder="Total" class="form-control aa" id="fri_result">
                            <label class="set_label">RPE:</label>
                            <input type="text" name="6st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="fri_rpe">
                        </td>

                        <td><span style="float: left; font-size: 14px;">Topic With Dur</span>
                            <br>
                            <label class="set_label">Warm Up:</label>
                            <input type="text" name="7st_sun_warm_up" placeholder="Warm-up" class="form-control aa" id="sat_intTextBox">
                            <label class="set_label">Physical Part:</label>
                            <input type="text" name="7st_sun_phy_part" placeholder="Phy-part" class="form-control aa" id="sat_phy_part">
                            <label class="set_label">Coach Part:</label>
                            <input type="text" name="7st_sun_coach_part" placeholder="Coach-part" class="form-control aa" id="sat_coach_part">
                            <label class="set_label">Cool Down:</label>
                            <input type="text" name="7st_sun_cool_part" placeholder="Cool-Down" class="form-control aa" id="sat_cool_down">
                            <label class="set_label">Total:</label>
                            <input type="text" name="7st_sun_total" placeholder="Total" class="form-control aa" id="sat_result">
                            <label class="set_label">RPE:</label>
                            <input type="text" name="7st_sun_rpe_part" placeholder="RPE" class="form-control aa" id="sat_rpe">
                        </td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Tactical</td>
                            <td><input type="text" name="tactical_first" id="tactical_first" class="form-control"></td>
                            <td><input type="text" name="tactical_scnd" id="tactical_scnd" class="form-control"></td>
                            <td><input type="text" name="tactical_third" id="tactical_third" class="form-control"></td>
                            <td><input type="text" name="tactical_fourth" id="tactical_fourth" class="form-control"></td>
                            <td><input type="text" name="tactical_fifth" id="tactical_fifth" class="form-control"></td>
                            <td><input type="text" name="tactical_six" id="tactical_six" class="form-control"></td>
                            <td><input type="text" name="tactical_seven" id="tactical_seven" class="form-control"></td>
                        </tr>
                            <tr>
                            <td class="first_tb_data">technical</td>
                            <td><input type="text" name="technical_first" id="technical_first" class="form-control"></td>
                            <td><input type="text" name="technical_scnd" id="technical_scnd" class="form-control"></td>
                            <td><input type="text" name="technical_third" id="technical_third" class="form-control"></td>
                            <td><input type="text" name="technical_fourth" id="technical_fourth" class="form-control"></td>
                            <td><input type="text" name="technical_fifth" id="technical_fifth" class="form-control"></td>
                            <td><input type="text" name="technical_six" id="technical_six" class="form-control"></td>
                            <td><input type="text" name="technical_seven" id="technical_six" class="form-control"></td>
                        </tr>
                        <tr>
                            <td class="first_tb_data">Mental</td>
                            <td><input type="text" name="mental_first" id="mental_first" class="form-control"></td>
                            <td><input type="text" name="mental_scnd" id="mental_scnd" class="form-control"></td>
                            <td><input type="text" name="mental_third" id="mental_third" class="form-control"></td>
                            <td><input type="text" name="mental_fourth" id="mental_fourth" class="form-control"></td>
                            <td><input type="text" name="mental_fifth" id="technical_fifth" class="form-control"></td>
                            <td><input type="text" name="mental_six" id="technical_six" class="form-control"></td>
                            <td><input type="text" name="mental_seven" id="mental_seven" class="form-control"></td>
                        </tr>
                    </tbody>
                </table>
                
            </form>
            </div>
        </section>
   <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    
    <script src="js/jquery.simplecolorpicker.js"></script>
   <script type="text/javascript">
       $(function() {
        $("#no-year-datepicker").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#monday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#tuesday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#wed_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#thursday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#friday_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
         $("#sat_date").datepicker({
        dateFormat: "d",
        changeMonth: true,
        changeYear: false
        });
        $("#new_date").datepicker({
        dateFormat: "mm",
        changeMonth: true,
        showButtonPanel: false,
        startView: "month", 
        minView: "month"
        });
});
   </script>
   <script type="text/javascript">
       $('select[name="sun_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
       $('select[name="mon_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
       $('select[name="tues_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
        $('select[name="wed_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});

         $('select[name="thu_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
         $('select[name="fri_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
         $('select[name="sat_color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
   </script>
   <script type="text/javascript">
    function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  });
}


setInputFilter(document.getElementById("sun_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sun_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//monday
setInputFilter(document.getElementById("mon_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("mon_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//Tuesday
setInputFilter(document.getElementById("tues_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("tues_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });
//Wednesday
setInputFilter(document.getElementById("wed_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("wed_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });

//Thursday
setInputFilter(document.getElementById("thu_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("thu_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });
//Friday


setInputFilter(document.getElementById("fri_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("fri_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });


setInputFilter(document.getElementById("sat_intTextBox"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_phy_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_coach_part"), function(value) {
  return /^-?\d*$/.test(value); });
setInputFilter(document.getElementById("sat_cool_down"), function(value) {
  return /^-?\d*$/.test(value); });



   </script>
   <script type="text/javascript">
       $('#sun_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            //console.log(myVar);
        });
        $('#sun_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
        });
        $('#sun_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            
        });
       $('#sun_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#sun_result').val(sun_answer);

        });
       $('#mon_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
        });
        $('#mon_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            //console.log(myVar);
        });
        $('#mon_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            //console.log(myVar);
            //alert(answer)
        });
       $('#mon_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#mon_result').val(sun_answer);

        });
       
        $('#tues_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            
        });
        $('#tues_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            
        });
        $('#tues_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            
        });
       $('#tues_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#tues_result').val(sun_answer);

        });

     
       $('#wed_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            
        });
        $('#wed_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            
        });
        $('#wed_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            
        });
       $('#wed_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#wed_result').val(sun_answer);

        });

       $('#thu_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
        });
        $('#thu_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
        });
        $('#thu_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            
        });
       $('#thu_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#thu_result').val(sun_answer);

        });

       

       $('#fri_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
        });
        $('#fri_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
            
        });
        $('#fri_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
        });
       $('#fri_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#fri_result').val(sun_answer);

        });
       //Sat
       $('#sat_intTextBox').keyup(function () {
            sun_warm_up = $(this).val();
            
        });
        $('#sat_phy_part').keyup(function () {
            sun_phy_part = $(this).val();
        });
        $('#sat_coach_part').keyup(function () {
            sun_coach_part = $(this).val();
            
        });
       $('#sat_cool_down').keyup(function () {
            sun_cool_down = $(this).val();
            sun_answer = parseInt(sun_warm_up, 10)+parseInt(sun_phy_part, 10)+parseInt(sun_coach_part, 10)+parseInt(sun_cool_down, 10);
            $('#sat_result').val(sun_answer);

        });
   </script>
   <!-- <script type="text/javascript">
    $(".save").on("click",function(){
        

    season =$("#season").val();
    age_group=$("#age_group").val();
    week =$("#week").val();
    new_date=$("#new_date").val();


    sun_date =$("#no-year-datepicker").val();
    monday_date=$("#monday_date").val();
    tuesday_date =$("#tuesday_date").val();
    wed_date=$("#wed_date").val();

    thursday_date =$("#thursday_date").val();
    friday_date=$("#friday_date").val();
    sat_date =$("#sat_date").val();
    //color
    sun_color=$("#sun_color").val();
    mon_color=$("#mon_color").val();
    tues_color=$("#tues_color").val();
    wed_color=$("#wed_color").val();
    thu_color=$("#thu_color").val();
    fri_color=$("#fri_color").val();
    sat_color=$("#sat_color").val();

    //1st Aim

    first_sun_aim=$("#1st_sun_aim").val();
    first_mon_aim=$("#1st_mon_aim").val();
    first_tue_aim=$("#1st_tue_aim").val();
    first_wed_aim=$("#1st_wed_aim").val();
    first_thu_aim=$("#1st_thu_aim").val();
    first_fri_aim=$("#1st_fri_aim").val();
    first_sat_aim=$("#1st_sat_aim").val();

    //2nd Aim
    second_sun_aim=$("#2st_sun_aim").val();
    second_mon_aim=$("#2st_mon_aim").val();
    second_tue_aim=$("#2st_tue_aim").val();
    second_wed_aim=$("#2st_wed_aim").val();
    second_thu_aim=$("#2st_thu_aim").val();
    second_fri_aim=$("#2st_fri_aim").val();
    second_sat_aim=$("#2st_sat_aim").val();

    //Detail
    sun_comment=$("#sun_comment").val();
    mon_comment=$("#mon_comment").val();
    tue_comment=$("#tue_comment").val();
    wed_comment=$("#wed_comment").val();
    thu_comment=$("#thu_comment").val();
    fri_comment=$("#fri_comment").val();
    sat_comment=$("#sat_comment").val();

    // over all sunday detail
    sun_intTextBox=$("#sun_intTextBox").val();
    sun_phy_part=$("#sun_phy_part").val();
    sun_coach_part=$("#sun_coach_part").val();
    sun_cool_down=$("#sun_cool_down").val();
    sun_result=$("#sun_result").val();
    sun_rpe=$("#sun_rpe").val();


    // over all Monday detail
    mon_intTextBox=$("#mon_intTextBox").val();
    mon_phy_part=$("#mon_phy_part").val();
    mon_coach_part=$("#mon_coach_part").val();
    mon_cool_down=$("#mon_cool_down").val();
    mon_result=$("#mon_result").val();
    mon_rpe=$("#mon_rpe").val();

    // over all Tuesday detail
    tues_intTextBox=$("#tues_intTextBox").val();
    tues_phy_part=$("#tues_phy_part").val();
    tues_coach_part=$("#tues_coach_part").val();
    tues_cool_down=$("#tues_cool_down").val();
    tues_result=$("#tues_result").val();
    tues_rpe=$("#tue_rpe").val();

    // over all Wednesday detail
    wed_intTextBox=$("#wed_intTextBox").val();
    wed_phy_part=$("#wed_phy_part").val();
    wed_coach_part=$("#wed_coach_part").val();
    wed_cool_down=$("#wed_cool_down").val();
    wed_result=$("#wed_result").val();
    wed_rpe=$("#wed_rpe").val();

    // over all Thursdau detail
    thu_intTextBox=$("#thu_intTextBox").val();
    thu_phy_part=$("#thu_phy_part").val();
    thu_coach_part=$("#thu_coach_part").val();
    thu_cool_down=$("#thu_cool_down").val();
    thu_result=$("#thu_result").val();
    thu_rpe=$("#thu_rpe").val();


    // over all Friday detail
    fri_intTextBox=$("#fri_intTextBox").val();
    fri_phy_part=$("#fri_phy_part").val();
    fri_coach_part=$("#fri_coach_part").val();
    fri_cool_down=$("#fri_cool_down").val();
    fri_result=$("#fri_result").val();
    fri_rpe=$("#fri_rpe").val();


    // over all Sat detail
    sat_intTextBox=$("#sat_intTextBox").val();
    sat_phy_part=$("#sat_phy_part").val();
    sat_coach_part=$("#sat_coach_part").val();
    sat_cool_down=$("#sat_cool_down").val();
    sat_result=$("#sat_result").val();
    sat_rpe=$("#sat_rpe").val();
    

    $.ajax({
        url:'insert_plan.php',
        processData: false,
        method: 'POST',
         data:{
         season:season,
         age_group:age_group,
         week:week,
         month:new_date,
         sun_date:sun_date,
         monday_date:monday_date,
         tuesday_date:tuesday_date,
         wed_date:wed_date,
         thursday_date:thursday_date,
         friday_date:friday_date,
        sat_date:sat_date,
        sun_color:sun_color,
        mon_color:mon_color,
        tues_color:tues_color,
        wed_color:wed_color,
        thu_color:thu_color,
        fri_color:fri_color,
        sat_color:sat_color,
        first_sun_aim:first_sun_aim,
        first_mon_aim:first_mon_aim,
        first_tue_aim:first_tue_aim,
        first_wed_aim:first_wed_aim,
        first_thu_aim:first_thu_aim,
        first_fri_aim:first_fri_aim,
        first_sat_aim:first_sat_aim,
        second_sun_aim:second_sun_aim,
        second_mon_aim:second_mon_aim,
        second_tue_aim:second_tue_aim,
        second_wed_aim:second_wed_aim,
        second_thu_aim:second_thu_aim,
        second_fri_aim:second_fri_aim,
        second_sat_aim:second_sat_aim,
        sun_comment:sun_comment,
        mon_comment:mon_comment,
        tue_comment:tue_comment,
        wed_comment:wed_comment,
        thu_comment:thu_comment,
        fri_comment:fri_comment,
        sat_comment:sat_comment,
        sun_intTextBox:sun_intTextBox,
        sun_phy_part:sun_phy_part,
        sun_coach_part:sun_coach_part,
        sun_cool_down:sun_cool_down,
        sun_result:sun_result,
        sun_rpe:sun_rpe,
        tues_intTextBox:tues_intTextBox,
        tues_phy_part:tues_phy_part,
        tues_coach_part:tues_coach_part,
        tues_cool_down:tues_cool_down,
        tues_result:tues_result,
        tues_rpe:tues_rpe,
        wed_intTextBox:wed_intTextBox,
        wed_phy_part:wed_phy_part,
        wed_coach_part:wed_coach_part,
        wed_cool_down:wed_cool_down,
        wed_result:wed_result,
        wed_rpe:wed_rpe,
        thu_intTextBox:thu_intTextBox,
        thu_phy_part:thu_phy_part,
        thu_coach_part:thu_coach_part,
        thu_cool_down:thu_cool_down,
        thu_result:thu_result,
        thu_rpe:thu_rpe,
        fri_intTextBox:fri_intTextBox,
        fri_phy_part:fri_phy_part,
        fri_coach_part:fri_coach_part,
        fri_cool_down:fri_cool_down,
        fri_result:fri_result,
        fri_rpe:fri_rpe,
        sat_intTextBox:sat_intTextBox,
        sat_phy_part:sat_phy_part,
        sat_coach_part:sat_coach_part,
        sat_cool_down:sat_cool_down,
        sat_result:sat_result,
        sat_rpe:sat_rpe,
        form_data:form_data
        },
        success: function(response){
            console.log(response);
            }
        });
  });
  </script>
  <script>
    $('.upload').on('click', function() {
    season =$("#season").val();
    var file_data = $('#file').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('file', file_data);                         
    $.ajax({
        url: 'insert_weekly_plan.php', // point to server-side PHP script 
        // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        season:season,                         
        type: 'post',
        success: function(php_script_response){
            //alert(php_script_response); // display response from the PHP script, if any
            console.log(php_script_response);
        }
     });
});
</script> -->

<script>
 // $(document).on("click","#save",function(e){  
 //     //Prevent Instant Click  
 //    e.preventDefault();
 //    // Create an FormData object 
 //        var formData =$("#plan_data").submit(function(e){
 //            return ;
 //        });
 //      //formData[0] contain form data only 
 //      // You can directly make object via using form id but it require all ajax operation inside $("form").submit(<!-- Ajax Here   -->)
 //        var formData = new FormData(formData[0]);    
 //        $.ajax({
 //            url: 'insert_weekly_plan.php',
 //            type: 'POST',
 //            data: formData,
 //            success: function(response) {console.log(response);},
 //            contentType: false,
 //            processData: false,
 //            cache: false
 //        });
 //        return false;
 //            });
</script>
</div>
</body>
</html>


<!-- <li class="nav-item newdropdown">
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
        <li class="nav-item newdropdown">
        <select class="form-control" name="month">
            <option selected="selected">Month</option>
            <option value="january">January</option>
            <option value="February">February</option>
            <option value="March">March</option>
            <option value="April">April</option>
            <option value="May">May</option>
            <option value="June">June</option>
            <option value="July">July</option>
            <option value="August">August</option>
            <option value="September">September</option>
            <option value="October">October</option>
            <option value="November">November</option>
            <option value="December">December</option>

        </select>
            </li> -->
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Perspective Page View Navigation</title>
		<meta name="description" content="Perspective Page View Navigation: Transforms the page in 3D to reveal a menu" />
		<meta name="keywords" content="3d page, menu, navigation, mobile, perspective, css transform, web development, web design" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<!-- csstransforms3d-shiv-cssclasses-prefixed-teststyles-testprop-testallprops-prefixes-domprefixes-load --> 
		<script src="js/modernizr.custom.25376.js"></script>
	</head>
	<body>
		<div id="perspective" class="perspective effect-airbnb">
			<div class="container">
				<div class="wrapper"><!-- wrapper needed for scroll -->
					<!-- Top Navigation -->
					<div class="codrops-top clearfix">
						<!-- <span class="right"><a class="codrops-icon codrops-icon-drop" href="http://tympanus.net/codrops/?p=17915"><span>Back to the Codrops Article</span></a></span> -->
					</div>
					<header class="codrops-header">
						<!-- <h1>Pro FootBall Coach</span></h1> -->	
						<img src="images/logo.png" class="img-responsive" style="width: 250px;
height: auto;">
					</header>
					<div class="main clearfix">
						<div class="column">
							<p style="text-align: center;
    margin: 100px auto;"><button id="showMenu">Menu</button></p>
						</div>
						<div class="column">
							<img src="images/football.jpg" class="img-responsive">
						</div>
						<!-- <div class="related">
							<p>If you enjoyed this demo you might also like:</p>
							<p><a href="http://tympanus.net/Tutorials/AnimatedBorderMenus/">Animated Border Menus</a></p>
							<p><a href="http://tympanus.net/Development/SidebarTransitions/">Transitions for Off-Canvas Navigations</a></p>
						</div> -->
					</div><!-- /main -->
				</div><!-- wrapper -->
			</div><!-- /container -->
			<nav class="outer-nav left vertical">
				<a href="weekly_plan.php" class="">Weekly Plan</a>
				  <a href="fetch_plan.php" class="">View Weekly Plan</a>
				<a href="player_form.php" class="">Player Evaluation Form</a>
				   <a href="fetch_player_form.php" class=""> View Player Evaluation Form</a>
				<a href="player_info_detail.php" class="">Add Player Info</a>
				  <a href="fetch_player_info.php" class="">View Player Info</a>
			</nav>
		</div><!-- /perspective -->
		<script src="js/classie.js"></script>
		<script src="js/menu.js"></script>
	</body>
</html>
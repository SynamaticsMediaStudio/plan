<?php
include_once("secure.php");
include('dbbridge/top.php');
  error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Player Eveluation Form</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/player_eveluation_form.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>


<body class="player_form">
    <section class="nav_section new_content" style="overflow: inherit;     min-height: 150vh;" id="new_content">
        <div class="container-fluid">
    <?php
    include('side_nav.php');
    ?>
    <div class="container">
      <form method="Post" action="update_page.php" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <input type="hidden" value="1">
                <label for="pwd" class="mr-sm-2">Player Number:</label>
                <br>
               <!--  <input type="text" class="form-control" id="name"> -->
            <select class="form-control" id="player_num" name="player_num">
                <option selected="selected">choose</option>
            <?php
            include('dbbridge/top.php');
            $db = new DBManager();
              $play_name_query="SELECT  * FROM player_info";
               $play_name_result=$db->sample($play_name_query);
               //print_r($result);
               foreach ($play_name_result as $key => $play_name_value) {
                # code...
                 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";

                $player_service_provider=$play_name_value['player_number'];
                if(in_array($player_service_provider, $player_repeated_data)==0)
                {
                 echo "<option class='dropdown-item ropdown-item month' id='saeson_anc' value=".$play_name_value['player_number'].">".$play_name_value['player_number']."</option>";
                }
                $player_repeated_data[] = $play_name_value['player_number'];
               }
              ?>
                </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <label for="pwd" class="mr-sm-2">Name:</label>
                <br>
               <!--  <input type="text" class="form-control" id="name"> -->
            <select class="form-control" id="player_name" name="player_name">
              <option selected="selected">choose</option>
            
                </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
            <label for="pwd" class="mr-sm-2">Father Name:</label>
            <br>
            <!-- <input type="text" class="form-control" id="Nationality" name="Nationality"> -->

            <select class="form-control" id="father_name" name="father_name">
            <option>choose</option>
              
                </select>
           </div>
        </div>
        <div class="row">
            <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group">
                    <label for="sel1">Choose:</label>
                    <select class="form-control" id="sel1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                </select>
                </div> 
                
            </div> -->
           
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
           <!--  <input type="text" class="form-control" id="Position" name="Position"> -->
           <div class="form-group">
                <label for="sel1">Nationality:</label>
                <br>
                <select class="form-control" id="Nationality" name="nationality">
                  <option selected="selected">choose</option>
                </select>
            </div> 
           </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
                <div class="form-group">
                <label for="sel1">Position:</label>
                <br>
                <select class="form-control" id="position" name="position">
                  <option selected="selected">choose</option>
            
                </select>
            </div> 
            </div>
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
            <label for="pwd" class="mr-sm-2">Evaluation:</label>
            <br>
            
            <!-- <input type="text" class="form-control" id="Academy" name="Academy"> -->
            <select class="form-control" id="eval" name="eval">
              <option selected="selected">choose</option>
            
                </select>
           </div>

        </div>
        <div class="row">
           
            <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="form-group">
                    <label for="sel1">Choose:</label>
                    <select class="form-control" id="sel1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                </select>
                </div> 
                
            </div> -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
            <label for="pwd" class="mr-sm-2">Academy:</label>
            <br>
           <!--  <input type="text" class="form-control" id="Position" name="Position"> -->
           <!-- <input type="text" class="form-control" id="national_team" name="national_team"> -->

           <select class="form-control" id="academy" name="academy">
            <option selected="selected">choose</option>
            
            </select>

           </div>
           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
                <div class="form-group">
                    <label for="sel1">National Team:</label>
                    <br>
                   <!--  <input type="text" class="form-control" id="Contact" name="Contact"> -->
                   <select class="form-control" id="national_team" name="national_team">
                    <option selected="selected">choose</option>
            
                </select>
                </div> 
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
                <div class="form-group">
                    <label for="sel1">Contact Number:</label>
                    <br>
                    <!-- <input type="text" class="form-control" id="f_Contact" name="f_Contact"> -->

            <select class="form-control" id="contact_num" name="contact_num">
              <option selected="selected">choose</option>
            
                </select>
                </div>
        </div>

           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
            <div class="form-group">
                <label for="sel1">Father Contact:</label>
                <br>
                    <select class="form-control" id="father_contact" name="father_contact">
                      <option selected="selected">choose</option>
            
                </select>
            </div> 
           
           </div>
           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
                <label for="sel1">Foot Infortmaion:</label>
                <br>
                <select class="form-control" id="foot_info" name="foot_info">
                  <option selected="selected">choose</option>
            
                </select>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="" class="player_image" name="player_image" style="margin: 18px 0 0 0;padding: 0;">
                <input type="hidden" name="new_player_image" id="new_player_image">
             </div>
        </div>

        <div class="row">
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
                <label for="sel1">Category:</label>
                <br>
                <select class="form-control" id="cat" name="cat">
                  <option selected="selected">choose</option>
            <?php
            include('dbbridge/top.php');
            $db = new DBManager();
              $play_cat_name_query="SELECT * FROM player_info";
               $play_cat_name_result=$db->sample($play_cat_name_query);
               //print_r($result);
               foreach ($play_cat_name_result as $key => $play_cat_name_value) {
                # code...
                 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";

                $player_cat_service_provider=$play_cat_name_value['fld_cat'];
                if(in_array($player_cat_service_provider, $player_cat_repeated_data)==0)
                {
                 echo "<option class='dropdown-item ropdown-item month' id='saeson_anc' value=".$play_cat_name_value['fld_cat'].">".$play_cat_name_value['fld_cat']."</option>";
                }
                $player_cat_repeated_data[] = $play_cat_name_value['fld_cat'];
               }
              ?>
                </select>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
                <label for="sel1">Season:</label>
                <br>
                <select class="form-control" id="seas" name="seas">
                  <option selected="selected">choose</option>
            <?php
            include('dbbridge/top.php');
            $db = new DBManager();
              $play_seas_name_query="SELECT * FROM player_info";
               $play_seas_name_result=$db->sample($play_seas_name_query);
               //print_r($result);
               foreach ($play_seas_name_result as $key => $play_seas_name_value) {
                # code...
                 //echo "<option value=".$value['fld_season'].">".$value['fld_season']."</option>";

                $player_seas_service_provider=$play_seas_name_value['fld_season'];
                if(in_array($player_seas_service_provider, $player_seas_repeated_data)==0)
                {
                 echo "<option class='dropdown-item ropdown-item month' id='saeson_anc' value=".$play_seas_name_value['fld_season'].">".$play_seas_name_value['fld_season']."</option>";
                }
                $player_seas_repeated_data[] = $play_seas_name_value['fld_season'];
               }
              ?>
                </select>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
             </div>
        </div>
        <!--  -->
  <div class="row">      
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4>Tactical</h4>
        <div class="multi-field-wrapper">
          <div class="multi-fields">
            <div class="new-multi-field">
              
                  <!-- <label>Title</label> -->
                  <!-- <br>
                  <input type="text" name="tac_eng_title[]" class="form-control">
                  <label>Arabic Title</label>
                  <br>
                  <input type="text" name="tac_arabic[]" class="form-control">
                  <label>Evaluation</label>
                  <br>
                  <input type="text" name="tac_eval[]" class="eval_set form-control">
              <button type="button" class="remove-field btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i></button>
      </div>
    </div>
    <button type="button" class="add-field btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> -->
<!-- </button> -->
      </div>
  </div>
</div>
</div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4>Technical</h4>
        <div class="multi-field-wrapper">
          <div class="multi-fields">
            <div class="tech-multi-field">
<!--                   <label>Title</label>
                  <br>
                  <input type="text" name="tech_eng_title[]" class="form-control">
                  <label>Arabic Title</label>
                  <br>
                  <input type="text" name="tech_arabic[]" class="form-control">
                  <label>Evaluation</label>
                  <br>
                  <input type="text" name="tech_eval[]" class="eval_set form-control">
                  <button type="button" class="remove-field btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i></button> -->
            </div>
          </div>
        <!-- <button type="button" class="add-field btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button> -->
      </div>
  </div>
  </div>
  <div class="row">      
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4>Physical</h4>
        <div class="multi-field-wrapper">
          <div class="multi-fields">
            <div class="phy-multi-field">
                  <!-- <label>Title</label>
                  <br>
                  <input type="text" name="phy_eng_title[]" class="form-control">
                  <label>Arabic Title</label>
                  <br>
                  <input type="text" name="phy_arabic[]" class="form-control">
                  <label>Evaluation</label>
                  <br>
                  <input type="text" name="phy_eval[]" class="eval_set form-control">
                  <button type="button" class="remove-field btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i></button> -->
      </div>
    </div>
    <!-- <button type="button" class="add-field btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button> -->
      </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4>Psychosocial</h4>
        <div class="multi-field-wrapper">
          <div class="multi-fields">
            <div class="psy-multi-field">
              <!-- <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label>Title</label>
                  <br>
                  <input type="text" name="psy_eng_title[]" class="form-control">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <label>Arabic Title</label>
                  <br>
                  <input type="text" name="psy_arabic[]" class="form-control">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <label>Evaluation</label>
              <br>
              <input type="text" name="psy_eval[]" class="eval_set form-control">
              <button type="button" class="remove-field btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i></button> -->
            </div>
          </div>
        <!-- <button type="button" class="add-field btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button> -->
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p><strong class="text-danger">Remarks:</strong></p>
        <div class="new_remarks_Area">
        <!-- <textarea cols="3" rows="2" class="form-control" name="remarks"></textarea> -->
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>Pro Football Player</label>
        <select class="form-control set_dynamic_text" name="player_sel" id="player_sel">
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>
      </div>
  </div>
  </div>
<input type="submit" class="btn btn-info" value="Update" style="margin: 23px 0 25px 0;">
<input type="submit" class="btn btn-danger" value="Delete" style="margin: 23px 0 25px 0;" id="del_id">
<input type="button" class="btn btn-primary" value="Download" style="margin: 23px 0 25px 0;" id="cmd">
    </form>
  </div>
  <div class="footer"><p>Document Preppared By Pro Football Coach.</p>
            <p>0097466053410 Or 0097477080967</p>
            <p>coachredouane@hotmail.com</p>
        </div>
</section>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script src="js/jquery.simplecolorpicker.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    <script type='text/javascript' src='js/pdfmake.min.js'></script>
    <script type='text/javascript' src='js/html2canvas.min.js'></script>
    <script type="text/javascript">
        
    $('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
      //alert('hi');
        if ($('.multi-field', $wrapper).length > 1)
        {
          //alert('bye');
            $(this).parent('.multi-field').remove();
        }
    });
});
    </script>
<script>
 // $(document).on("click","#save",function(e){  
 //     //Prevent Instant Click  
 //    e.preventDefault();
 //    // Create an FormData object 
 //        var formData =$("#plan_data").submit(function(e){
 //            return ;
 //        });
 //      //formData[0] contain form data only 
 //      // You can directly make object via using form id but it require all ajax operation inside $("form").submit(<!-- Ajax Here   -->)
 //        var formData = new FormData(formData[0]);    
 //        $.ajax({
 //            url: 'insert_weekly_plan.php',
 //            type: 'POST',
 //            data: formData,
 //            success: function(response) {console.log(response);},
 //            contentType: false,
 //            processData: false,
 //            cache: false
 //        });
 //        return false;
 //            });
 $('#player_num').change(function(){
    //alert('hi');
    var action=3;
    var player_num=$('#player_num').val();

    // var player_name=$('#player_name').val();
    // var father_name=$('#father_name').val();
    // var Nationality=$('#Nationality').val();
    // var evaluation=$('#eval').val();
    // var academy=$('#academy').val();
    // var national_team=$('#national_team').val();
    // var contact_num=$('#contact_num').val();
    // var father_contact=$('#father_contact').val();
    // var foot_info=$('#foot_info').val();
    // alert(player_num);
    // alert(player_name);
    // alert(father_name);
    // alert(Nationality);
    // alert(evaluation);
    // alert(national_team);
    // alert(contact_num);
    // alert(father_contact);
    // alert(foot_info);
    // exit();
    //alert(player_num);
    $.ajax({
      type:'POST',
     url:'live_search.php',
      data:{
        dataType: 'json',
        encode: true,
        player_num:player_num,
        action:action,
      },
    })
      .done(function(value){
      var data = value.split(",");
      console.log(data[12]);
      // exit();
      //$(".player_image").prop("src",data[0]);
      $("#player_name").html(data[0]);
      $("#father_name").html(data[1]);
      $("#Nationality").html(data[2]);
      $("#position").html(data[3]);
      $("#eval").html(data[4]);
      $("#academy").html(data[5]);
      $("#national_team").html(data[6]);
      $("#contact_num").html(data[7]);
      $("#father_contact").html(data[8]);
      $("#foot_info").html(data[9]);
      $("#cat").html(data[10]);
      $("#seas").html(data[11]);
      $(".player_image").prop("src",data[12]);
      // $(".multi-field").append("<input name='example2' value="+data[13]+" type='text'/>")
      $(".new-multi-field").html(data[13]);
      // $(".new-multi-field").html(data[13]);
      // $(".new-multi-field").html(data[13]);
      $(".tech-multi-field").html(data[14]);
      $(".phy-multi-field").html(data[15]);
      $(".psy-multi-field").html(data[16]);
      $(".new_remarks_Area").html(data[17]);
      $(".player_sel").html(data[18]);

    });
 });
</script>
<script type="text/javascript">
      $("body").on("click", "#cmd", function () {
          $('.nav-link').css('color', 'red !important');
          // var img=canvas.toDataURL("image/png");
          html2canvas($('.new_content')[0], {
              onrendered: function (canvas) {
                  var data = canvas.toDataURL();
                  var docDefinition = {
                      content: [{
                          image: data,
                          width: 500
                      }]
                  };
                  pdfMake.createPdf(docDefinition).download("Table.pdf");
              }
          });
           //$('.page-wrap').css({color: white});
          
      });
    </script>
<script>
  $('#del_id').click(function(){
    del_id =$('#player_num').val();
  alert(del_id);
  action=7;
  $.ajax({
      type:'POST',
     url:'live_search.php',
      data:{
        dataType: 'json',
        encode: true,
        del_id:del_id,
        action:action,
      },
    })
      .done(function(value){
      var data = value.split(",");
      console.log(data[12]);
      // exit();
      //$(".player_image").prop("src",data[0]);
      

    });
  });
  

</script>
</body>
</html>
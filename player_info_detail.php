<?php
include_once("secure.php");
  error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Player Eveluation Form</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/player_eveluation_form.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>


<body>
     <div class="container">
       <?php
    include('side_nav.php');
    ?>
        <h1>Player Information detail</h1>
        <form action="insert_player_info.php" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Player Number:</label>
                <input type="text" class="form-control" id="player_num" name="player_num">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <input type="file" class="file" name="file" id="file" required style="margin: 30px 0 0 0;">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Name:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Father Name:</label>
                <input type="text" class="form-control" id="name" name="f_name">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Nationality:</label>
                <input type="text" class="form-control" id="Nationality" name="Nationality">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Position:</label>
                <input type="text" class="form-control" id="position" name="position">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Evaluation:</label>
                <input type="text" class="form-control" id="Evaluation" name="Evaluation">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Academy:</label>
                <input type="text" class="form-control" id="Academy" name="Academy">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">National Team:</label>
                <input type="text" class="form-control" id="national_team" name="national_team">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">contact_number:</label>
                <input type="text" class="form-control" id="contact_number" name="contact_number">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Father Contact:</label>
                <input type="text" class="form-control" id="father_contact" name="father_contact">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Foot Info:</label>
                <input type="text" class="form-control" id="foot_info" name="foot_info">
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Category:</label>
                <input type="text" class="form-control" id="player_cat" name="player_cat">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label for="pwd" class="mr-sm-2">Season:</label>
                <input type="text" class="form-control" id="seas" name="seas">
            </div>
            
        </div>
        <br>
        <input type="submit" name="submit" value="Save" class="btn btn-primary" style="    margin: 0 0 150px 0;">

    </form>

    </div>
   <div class="footer"><p>Document Preppared By Pro Football Coach.</p>
            <p>0097466053410 Or 0097477080967</p>
            <p>coachredouane@hotmail.com</p>
        </div>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script src="js/jquery.simplecolorpicker.js"></script>
</body>
</html>
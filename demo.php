<?php
include_once("secure.php");
  error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Weekly Plan</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="css/style.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>


<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <?php
    include('side_nav.php');
    ?>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
          season
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="season" style="border: none;" required id="season">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Age Group
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <input type="text" class="form-control" name="age_group" style="border: none;" id="age_group">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdownMenuButton" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Week
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <input type="text" class="form-control" name="week" style="border: none;" required id="week">
        </div>
      </li>
      <li class="nav-item dropdown">
        <!-- <div class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
          Month
        </div> -->
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
        <!-- <input type="text" id="new_date" class="new_date form-control" style="border: none;" required  name="month"> -->
        <li class="nav-item dropdown">
        <select class="form-control" name="month">
            <option selected="selected">Month</option>
            <option value="january">January</option>
            <option value="February">February</option>
            <option value="March">March</option>
            <option value="April">April</option>
            <option value="May">May</option>
            <option value="June">June</option>
            <option value="July">July</option>
            <option value="August">August</option>
            <option value="September">September</option>
            <option value="October">October</option>
            <option value="November">November</option>
            <option value="December">December</option>

        </select>
            </li>
        </div>
      </li>
       
        <input type="file" class="file" name="file" id="file" required>
    </ul>
  </div>
</nav>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    
    <script src="js/jquery.simplecolorpicker.js"></script>
</body>
</html>
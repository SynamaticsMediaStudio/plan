<!DOCTYPE html>
<html lang="en">

<head>
    <title>Player Eveluation Form</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <!-- Google font-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-regularfont.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-glyphicons.css">
    <link rel="stylesheet" href="css/jquery.simplecolorpicker-fontawesome.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/side_nav.css" rel="stylesheet">
    <!-- Required Fremwork -->
    
    <!-- ico font -->
    
</head>


<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="weekly_plan.php" class="">Weekly Plan</a>
  <a href="fetch_plan.php" class="">View Weekly Plan</a>
  <a href="player_form.php" class="">Player Evaluation Form</a>
   <a href="fetch_player_form.php" class=""> View Player Evaluation Form</a>
  <a href="player_info_detail.php" class="">Add Player Info</a>
  <a href="fetch_player_info.php" class="">View Player Info</a>
</div>
<span style="font-size:30px;cursor:pointer" onclick="openNav()"><img src="images/Pro_Football_Coach_Logo.png" class="img-responsive set_logo"></span>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
   
</body>
</html> 
  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/popper.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script src="js/jquery.simplecolorpicker.js"></script>
</html>
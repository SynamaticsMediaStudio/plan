<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">

  <title>Pro Football Coach</title>

  <link rel="stylesheet" href="css/reset.css">

    <link rel="stylesheet" href="css/login_style.css" media="screen" type="text/css" />

</head>

<body>
	<form  name="myform" action="javascript:login()" enctype="multipart/form-data" method="post">
	  <div class="wrap">
	  	<p id="response"></p>
			
		<img src="images/logo.png" style="width: 250px;height: auto;margin: 0 0 17px 0;">
			
			<input type="text" placeholder="username" required id="username">
			<div class="bar">
				<i></i>
			</div>
			<input type="password" placeholder="password" required id="password">
			<br>
			<button>Sign in</button>
		</div>
	</form>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/index.js"></script>
	<script src="js/login.js"></script>

</body>

</html>